%{
#include "parser.tab.h"
#include "ast.h"

extern ValorLexico* criaValorLexico(TipoValorLexico tipo, int linha, char* valor);

int get_line_number();
%}

%option yylineno
DIGITO [0-9]+
INT {DIGITO}
IDENTIFICADOR ([a-zA-Z]|_)+([a-zA-Z]|[0-9]|_)*
%x COMMENT

%%
\n {}
"//".*  {}
[ \t\r]+ {}

"/*"    {BEGIN(COMMENT);}
<COMMENT>"*/" {BEGIN(INITIAL);}
<COMMENT>"\n" {}
<COMMENT>.


"int" { return TK_PR_INT; }
"float" { return TK_PR_FLOAT; }
"bool" { return TK_PR_BOOL; }
"char" { return TK_PR_CHAR; }
"string" { return TK_PR_STRING; }
"if" { yylval.valorLexico = criaValorLexico(OP_COMPOSTO, get_line_number(), yytext); return TK_PR_IF; }
"then" { return TK_PR_THEN; }
"else" { yylval.valorLexico = criaValorLexico(OP_COMPOSTO, get_line_number(), yytext); return TK_PR_ELSE; }
"while" { yylval.valorLexico = criaValorLexico(OP_COMPOSTO, get_line_number(), yytext); return TK_PR_WHILE; }
"do" { return TK_PR_DO; }
"input" { yylval.valorLexico = criaValorLexico(OP_COMPOSTO, get_line_number(), yytext); return TK_PR_INPUT; }
"output" { yylval.valorLexico = criaValorLexico(OP_COMPOSTO, get_line_number(), yytext); return TK_PR_OUTPUT; }
"return" { yylval.valorLexico = criaValorLexico(OP_COMPOSTO, get_line_number(), yytext); return TK_PR_RETURN; }
"const" { return TK_PR_CONST; }
"static" { return TK_PR_STATIC; }
"foreach" { return TK_PR_FOREACH; }
"for" { yylval.valorLexico = criaValorLexico(OP_COMPOSTO, get_line_number(), yytext); return TK_PR_FOR; }
"switch" { return TK_PR_SWITCH; }
"case" { return TK_PR_CASE; }
"break" { yylval.valorLexico = criaValorLexico(OP_COMPOSTO, get_line_number(), yytext); return TK_PR_BREAK; }
"continue" { yylval.valorLexico = criaValorLexico(OP_COMPOSTO, get_line_number(), yytext); return TK_PR_CONTINUE; }
"class" { return TK_PR_CLASS; }
"private" { return TK_PR_PRIVATE; }
"public" { return TK_PR_PUBLIC; }
"protected" { return TK_PR_PROTECTED; }
"end" { return TK_PR_END; }
"default" { return TK_PR_DEFAULT; }

"," { return yytext[0]; }
";" { return yytext[0]; }
":" { return yytext[0]; }
"(" { return yytext[0]; }
")" { return yytext[0]; }
"[" { return yytext[0]; }
"]" { return yytext[0]; }
"{" { return yytext[0]; }
"}" { return yytext[0]; }
"+" { yylval.valorLexico = criaValorLexico(CARACTER_ESPECIAL, get_line_number(), yytext); return yytext[0]; }
"-" { yylval.valorLexico = criaValorLexico(CARACTER_ESPECIAL, get_line_number(), yytext); return yytext[0]; }
"|" { yylval.valorLexico = criaValorLexico(CARACTER_ESPECIAL, get_line_number(), yytext); return yytext[0]; }
"*" { yylval.valorLexico = criaValorLexico(CARACTER_ESPECIAL, get_line_number(), yytext); return yytext[0]; }
"/" { yylval.valorLexico = criaValorLexico(CARACTER_ESPECIAL, get_line_number(), yytext); return yytext[0]; }
"<" { yylval.valorLexico = criaValorLexico(CARACTER_ESPECIAL, get_line_number(), yytext); return yytext[0]; }
">" { yylval.valorLexico = criaValorLexico(CARACTER_ESPECIAL, get_line_number(), yytext); return yytext[0]; }
"=" { yylval.valorLexico = criaValorLexico(CARACTER_ESPECIAL, get_line_number(), yytext); return yytext[0]; }
"!" { yylval.valorLexico = criaValorLexico(CARACTER_ESPECIAL, get_line_number(), yytext); return yytext[0]; }
"&" { yylval.valorLexico = criaValorLexico(CARACTER_ESPECIAL, get_line_number(), yytext); return yytext[0]; }
"%" { yylval.valorLexico = criaValorLexico(CARACTER_ESPECIAL, get_line_number(), yytext); return yytext[0]; }
"#" { yylval.valorLexico = criaValorLexico(CARACTER_ESPECIAL, get_line_number(), yytext); return yytext[0]; }
"^" { yylval.valorLexico = criaValorLexico(CARACTER_ESPECIAL, get_line_number(), yytext); return yytext[0]; }
"." { return yytext[0]; }
"$" { return yytext[0]; }
"?" { return yytext[0]; }

"<=" { yylval.valorLexico = criaValorLexico(OP_COMPOSTO, get_line_number(), yytext); return TK_OC_LE; }
">=" { yylval.valorLexico = criaValorLexico(OP_COMPOSTO, get_line_number(), yytext); return TK_OC_GE; }
"==" { yylval.valorLexico = criaValorLexico(OP_COMPOSTO, get_line_number(), yytext); return TK_OC_EQ; }
"!=" { yylval.valorLexico = criaValorLexico(OP_COMPOSTO, get_line_number(), yytext); return TK_OC_NE; }
"&&" { yylval.valorLexico = criaValorLexico(OP_COMPOSTO, get_line_number(), yytext); return TK_OC_AND; }
"||" { yylval.valorLexico = criaValorLexico(OP_COMPOSTO, get_line_number(), yytext); return TK_OC_OR; }
">>" { yylval.valorLexico = criaValorLexico(OP_COMPOSTO, get_line_number(), yytext); return TK_OC_SL; }
"<<" { yylval.valorLexico = criaValorLexico(OP_COMPOSTO, get_line_number(), yytext); return TK_OC_SR; }

false { yylval.valorLexico = criaValorLexico(LIT_BOOL, get_line_number(), "0"); return TK_LIT_FALSE; }
true { yylval.valorLexico = criaValorLexico(LIT_BOOL, get_line_number(), "1"); return TK_LIT_TRUE; }

'.' { yylval.valorLexico = criaValorLexico(LIT_CHAR, get_line_number(), yytext); return TK_LIT_CHAR; }
'..+' { return TOKEN_ERRO; }
'[^'|^\n]* { return TOKEN_ERRO; }

\"[^"]*\" { yylval.valorLexico = criaValorLexico(LIT_STRING, get_line_number(), yytext); return TK_LIT_STRING; }
\"[^"|^\n]* { return TOKEN_ERRO; }

{INT} { yylval.valorLexico = criaValorLexico(LIT_INT, get_line_number(), yytext); return TK_LIT_INT; }

{INT}\.{DIGITO}+((e|E){DIGITO})? { yylval.valorLexico = criaValorLexico(LIT_FLOAT, get_line_number(), yytext); return TK_LIT_FLOAT; }
{INT}\.[^{DIGITO}] { return TOKEN_ERRO; }

{IDENTIFICADOR} { yylval.valorLexico = criaValorLexico(TVL_IDENTIFICADOR, get_line_number(), yytext) ; return TK_IDENTIFICADOR; }
{DIGITO}{IDENTIFICADOR} { return TOKEN_ERRO; }

. { return TOKEN_ERRO; }
%%

int get_line_number() {
    return yylineno;
}