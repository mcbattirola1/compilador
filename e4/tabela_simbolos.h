#include "ast.h"

#define MAX_ARGS 50
#define MAX_SIMBOLOS 1000
#define MAX_PROFUNDIDADE 20
#define MAX_TIPOS_DESCONHECIDOS 20

typedef enum TipoDeDado {
    TIPO_INT = 1,
    TIPO_FLOAT = 2,
    TIPO_CHAR = 3,
    TIPO_STRING = 4,
    TIPO_BOOLEAN = 5
} TipoDeDado;

typedef enum Natureza {
    NAT_LITERAL = 1,
    NAT_VARIAVEL = 2,
    NAT_FUNCAO = 3,
    NAT_ARRAY = 4
} Natureza;

typedef struct Simbolo {
    char *chave; // nome do identificador (por exemplo)
    int linha;
    int coluna;
    Natureza natureza; // identificador, funcao, literal
    TipoDeDado tipo; // int, float...
    int tamanho;
    int qtdItems;
    char* chavesArgumentos[MAX_ARGS];
    TipoDeDado tiposArgumentos[MAX_ARGS];
    ValorLexico *valorLexico;
} Simbolo;

// indiceUltimoSimbolo aponta para onde tem um elemento
// incrementar antes de atribuir
typedef struct TabelaDeSimbolos {
    Simbolo* simbolos[MAX_SIMBOLOS];
    int indiceUltimoSimbolo;
} TabelaDeSimbolos;

// indiceUltimoEscopo aponta para onde tem um elemento
// incrementar antes de atribuir
typedef struct PilhaDeEscopos {
    TabelaDeSimbolos* escopo[MAX_PROFUNDIDADE];
    int indiceUltimoEscopo;
} PilhaDeEscopos;

void criaSimbolo(ValorLexico *valorLexico, int tipo, Natureza natureza, int qtdItems);
void criaSimboloSemTipo(ValorLexico *valorLexico, Natureza natureza, int qtdItems);
char* getChave(ValorLexico *valorLexico);
PilhaDeEscopos* getPilhaGlobal();
TabelaDeSimbolos* getEscopoAtual();
void adicionaTipoASimbolos(int tipo);
void adicionaArgumentoNaLista(int tipo);
void criaParametro(ValorLexico *valorLexico, int tipo, Natureza natureza, int qtdItems);
int chaveJaNaPilha(char* chave);
int chaveJaNoEscopo(char* chave, TabelaDeSimbolos* escopo);
int chaveJaNoEscopoAtual(char* chave);
void TerminaComErro(int tipoErro, char* chave, int linha);
TabelaDeSimbolos* getEscopo(int indice);
TabelaDeSimbolos* getEscopoAtual();
void pushEscopo();
void popEscopo();
void adicionaTipoASimbolos(int tipo);
void adicionaSimboloAoEscopoAtual(Simbolo* simbolo);
int infereTipo(int t1, int t2);
Simbolo* getSimboloDaPilha(char* chave);
Simbolo* getSimboloDoEscopo(char* chave, TabelaDeSimbolos* escopo);
int getTipo(ValorLexico* valorLexico);
int getTamanhoPorTipo(int tipo);
int tiposSaoCompativeis(int tipoEsperado, int tipoRecebido);
int infereTipoOpBinaria(Nodo* n1, Nodo* n2);
void setTipoDaFuncaoAtual(int tipo, ValorLexico* valorLexico);
void validaUsoIdentificador(ValorLexico* valorLexico);
void validaAtribuicao(ValorLexico* valorLexico, Nodo* nodoExpressao);
void validaSeEhArray(ValorLexico* valorLexico);
void validaSeEhVariavel(ValorLexico* valorLexico);
void validaSeEhFuncao(ValorLexico* valorLexico);
void validaArgumentos(ValorLexico* valorLexico);
void validaTipoInput(ValorLexico* valorLexico);
void validaTipoOutput(ValorLexico* valorLexico);
void validaTipoOutputLiteral(int tipo, int linha);
void validaTipoRetorno(int tipo, int linha);
void validaQtdShift(ValorLexico* valorLexico, int qtd);
void criaInicializacao(int tipo, ValorLexico* valor);
void validaAtribuicaoLitString(ValorLexico* valorLexico, ValorLexico* valorLexicoString);