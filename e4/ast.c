#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include "ast.h"

int currentVLIndex = 0;
ValorLexico* VLs[30000];

int currentNodoIndex = 0;
Nodo* Nodos[30000];

int currentStringsIndex = 0;
char* strings[30000];

/* filho1 = proximo */
Nodo* criaNodo(TipoNodo tipoNodo, ValorLexico* valorLexico,  Nodo* filho0, Nodo* filho1, Nodo* filho2, Nodo* filho3, Nodo* filho4) {
    Nodo* nodo;
    nodo = (Nodo*) calloc(1, sizeof(Nodo));

    nodo->tipo = tipoNodo;

    nodo->valorLexico = valorLexico;

    nodo->filhos[0] = filho0;
    nodo->filhos[1] = filho1;
    nodo->filhos[2] = filho2;
    nodo->filhos[3] = filho3;
    nodo->filhos[4] = filho4;

    Nodos[currentNodoIndex] = nodo;
    currentNodoIndex++;

    return nodo;
}

ValorLexico* criaValorLexico(TipoValorLexico tipo, int linha, char* valor) {
    ValorLexico* valorLexico;
    valorLexico = (ValorLexico*) calloc(1, sizeof(ValorLexico));

    valorLexico->tipo = tipo;
    valorLexico->linha = linha;

    switch (tipo) {
    case LIT_INT:
        valorLexico->valor.vInt = atoi(valor);
        break;
    case LIT_FLOAT:
        valorLexico->valor.vFloat = atof(valor);
        break;
    case LIT_CHAR: ;
        char* valChar = valor;
        // descarta o fecha aspas
        valChar[strlen(valChar)-1] = '\0';

        // descarta o abre aspas
        valChar += 1;
        valorLexico->valor.vChar = strdup(valChar);
        break;
    case LIT_STRING: ;
        char* val = valor;
        // descarta o fecha aspas
        val[strlen(val)-1] = '\0';

        // descarta o abre aspas
        val += 1;
        valorLexico->valor.vStr = strdup(val);
        break;   
    case LIT_BOOL: ;
        int valBool = 0;
        if (valor[0] == '1') {
            valBool = 1;
        }
        valorLexico->valor.vInt = valBool;
        break; 
    case OP_COMPOSTO:
    case CARACTER_ESPECIAL:
    case TVL_IDENTIFICADOR:
        valorLexico->valor.vStr = strdup(valor);
    default:
        break;
    }

    VLs[currentVLIndex] = valorLexico;
    currentVLIndex++;
    return valorLexico;    
}

void exporta(void *arvore) {
    struct Nodo *arvoreNode = (struct Nodo*) arvore;

    exportaEnderecos(arvoreNode);
    exportaLabels(arvoreNode);
}

void exportaEnderecos(Nodo *arvoreNode) {
    if (arvoreNode == NULL) {
        return;
    }

    for (int i = 0; i < MAX_FILHOS; i++)
    {
        if (arvoreNode->filhos[i] != NULL && arvoreNode->filhos[i] != 0) {
            printf("%p, %p\n", arvoreNode, arvoreNode->filhos[i]);
        }
    }

    for (int i = 0; i < MAX_FILHOS; i++)
    {
        if (arvoreNode->filhos[i] != NULL && arvoreNode->filhos[i] != 0) {
            exportaEnderecos(arvoreNode->filhos[i]);
        }
    }
}

void exportaLabels(Nodo *arvoreNode) {
    if (arvoreNode != NULL) {
        char* nomeTipo = getNomeTipoNodo(arvoreNode);
        printf("%p [label=\"%s\"]\n", arvoreNode, nomeTipo);

        for (int i = 0; i < MAX_FILHOS ; i++)
        {
            if (arvoreNode->filhos[i] != NULL) {
                exportaLabels(arvoreNode->filhos[i]);
            }
        }
    }
}

void libera(void *arvore) {
    for (int i = 0; i <= currentNodoIndex; i++)
    {
        if (Nodos[i] != NULL) {
            free(Nodos[i]);
        }
    }
    

    for (int i = 0; i <= currentVLIndex; i++)
    {
        if (VLs[i] != NULL) {
            switch(VLs[i]->tipo) {
                case OP_COMPOSTO:
                case CARACTER_ESPECIAL:
                case TVL_IDENTIFICADOR:
                case LIT_STRING:
                    free(VLs[i]->valor.vStr);
                    break;
                case LIT_CHAR:
                    free(VLs[i]->valor.vChar);
                    break;
            }
            free(VLs[i]);
        }        
    }    

    for (int i = 0; i <= currentStringsIndex; i++)
    {
        free(strings[i]);
    }

}

char *concatStrings(char* s1, char* s2){
    char *ns = malloc(strlen(s1) + strlen(s2) + 1);
    ns[0] = '\0';
    strcat(ns, s1);
    strcat(ns, s2);

    strings[currentStringsIndex] = ns;
    currentStringsIndex++;
    return ns;
}

char* getNomeTipoNodo(Nodo *arvoreNode) {
    switch (arvoreNode->tipo) {
    case LITERAL:
        return getLiteralLabel(arvoreNode);
    case FUNCAO:
        return arvoreNode->valorLexico->valor.vStr; // TODO: add ()
    case COMANDO:
        return arvoreNode->valorLexico->valor.vStr;
    case CHAMADA_FUNCAO: ;
        // char* primeiro;
        // primeiro = arvoreNode->valorLexico->valor.vStr;
        // char segundo[] = "()";
        // strcat(primeiro, segundo);
        // return primeiro;
        char* call = "call ";
        return concatStrings(call, arvoreNode->valorLexico->valor.vStr);
    case PROGRAMA:
        return "PROGRAMA";
    case IDENTIFICADOR:
        return arvoreNode->valorLexico->valor.vStr;
    case OP_UNARIA:
    case OP_BINARIA:
        return  arvoreNode->valorLexico->valor.vStr;
    case ATRIBUICAO:
        return  "=";
    case INICIALIZACAO:
        return "<=";
    case ACESSO_ARRAY:
        return "[]";
    default:
        return "NAO ACHEI";
    }
}

char* getLiteralLabel(Nodo *arvoreNode) {
    switch(arvoreNode->valorLexico->tipo) {
        case CARACTER_ESPECIAL:
            return arvoreNode->valorLexico->valor.vChar;
        case OP_COMPOSTO:
            return arvoreNode->valorLexico->valor.vStr;
        case TVL_IDENTIFICADOR:
            return arvoreNode->valorLexico->valor.vStr;
        case LIT_INT: ;
            char str[100];
            sprintf(str, "%d", arvoreNode->valorLexico->valor.vInt);
            char* ret = strdup(str);
            strings[currentStringsIndex] = ret;
            currentStringsIndex++;
            return ret; // aqui
        case LIT_FLOAT: ;
            char array[100];
            sprintf(array, "%f", arvoreNode->valorLexico->valor.vFloat);
            char* retF = strdup(array);
            strings[currentStringsIndex] = retF;
            currentStringsIndex++;
            return retF; //aqui
        case LIT_CHAR:
            return arvoreNode->valorLexico->valor.vChar;
        case LIT_STRING:
            return arvoreNode->valorLexico->valor.vStr;
        case LIT_BOOL: 
            if (arvoreNode->valorLexico->valor.vInt == 1) {
                return "true";
            }
            return "false";
    }
}