# TODO

## 2.2

[x] Verificar se identificador nao eh declarado duas vezes
[x] Verificar se identificador sendo usado ja foi declarado
[x] Permitir variaveis com o mesmo nome em escopos diferentes ou mais internos

## 2.3

[x] Variaveis nao podem ser usadas com indexacao
[x] Funcoes apenas sao usadas com chamadas (por exemplo: foo())
[x] Vetores apenas podem ser usados como vetores, com indexacao

## 2.4

[x] Situacoes onde coercao de strings aconteceriam, lanca erro ERR_STRING_TO_X
[x] Situacoes onde coercao de chars aconteceriam, lanca erro ERR_STRING_TO_X
[x] Vetores nao podem ser declarados com o tipo string

## 2.5

[x] Funcoes sao chamadas com o numero correto de argumentos
[x] Funcoes chamadas com o numero correto de argumentos devem ter os tipos corretos nso argumentos
[x] Retorno, argumento e parametro de funcoes nao sao do tipo string

## 2.6

[x] Nao pode atribuir um valor de tipo incompativel a um identificador
[x] Comando input eh seguido de identificador do tipo int ou float
[x] Comando output eh seguido por um identificador ou literal do tipo int ou float
[x] O return de uma funcao eh do mesmo tipo que ela foi declarada.
[x] Nos comandos de shift, lancar erro caso o parametro for um numero maior que 16

## 2.7

[x] Mensagens de erro significativas

## Regras de escopo

[x] Respeitar os escopos
[x] Eh possivel declarar variaveis com mesmo nome em escopos diferentes
[x] Nao eh possivel declarar variaveis com mesmo nome no mesmo nivel de escopo

## Conversao Implicita

[x] Nao ha conversao com strings e chars
[x] Tipo int pode ser convertido implicitamente para float e bool
[x] bool pode ser convertido para float e int
[x] float pode ser convertido para int e bool, perdendo precisao

## Inferencia de tipos

[x] int + int = int
[x] float + float = float
[x] bool + bool = bool
[x] float + int = float
[x] bool + int = int
[x] bool + float = float

## Tamanhos

[x] passar o tamanho de cada tipo
[x] em caso de arrays, multiplicar o tamanho do array pelo do tipo para ter tamanho total.

## Bonus

[ ] Melhorar msg de erro, passar val. lexico em vez de chave (permitindo usar chave e mais coisas)
[ ] Testar tudo, checklist:

### Checklist - 2.2

[x] Verificar se identificador nao eh declarado duas vezes
[x] Verificar se identificador sendo usado ja foi declarado
[x] Permitir variaveis com o mesmo nome em escopos diferentes ou mais internos

### Checklist - 2.3

[x] Variaveis nao podem ser usadas com indexacao
[x] Funcoes apenas sao usadas com chamadas (por exemplo: foo())
[x] Vetores apenas podem ser usados como vetores, com indexacao

### Checklist - 2.4

[ ] Situacoes onde coercao de strings aconteceriam, lanca erro ERR_STRING_TO_X
[ ] Situacoes onde coercao de chars aconteceriam, lanca erro ERR_STRING_TO_X
[x] Vetores nao podem ser declarados com o tipo string

### Checklist - 2.5

[x] Funcoes sao chamadas com o numero correto de argumentos
[x] Funcoes chamadas com o numero correto de argumentos devem ter os tipos corretos nso argumentos
[x] Retorno, argumento e parametro de funcoes nao sao do tipo string

### Checklist - 2.6

[x] Nao pode atribuir um valor de tipo incompativel a um identificador
[x] Comando input eh seguido de identificador do tipo int ou float
[x] Comando output eh seguido por um identificador ou literal do tipo int ou float
[x] O return de uma funcao eh do mesmo tipo que ela foi declarada.
[x] Nos comandos de shift, lancar erro caso o parametro for um numero maior que 16

### Checklist - 2.7

[x] Mensagens de erro significativas

### Checklist - Regras de escopo

[x] Respeitar os escopos
[x] Eh possivel declarar variaveis com mesmo nome em escopos diferentes
[x] Nao eh possivel declarar variaveis com mesmo nome no mesmo nivel de escopo

### Checklist - Conversao Implicita

[x] Nao ha conversao com strings e chars
[x] Tipo int pode ser convertido implicitamente para float e bool
[x] bool pode ser convertido para float e int
[x] float pode ser convertido para int e bool, perdendo precisao

### Checklist - Inferencia de tipos

[x] int + int = int
[x] float + float = float
[x] bool + bool = bool
[x] float + int = float
[x] bool + int = int
[x] bool + float = float

### Checklist - Tamanhos

[ ] passar o tamanho de cada tipo
[ ] em caso de arrays, multiplicar o tamanho do array pelo do tipo para ter tamanho total.