// 2.5 Retorno, argumentos e parametros de funcoes
/* Retorno, argumentos e parametros de funcoes nao podem ser do tipo
string. Quando estes casos acontecerem, lancar o erro ERR_FUNCTION_STRING. */

int a;
float c;
bool func(int e, bool ashasduhsd, string z) { // ERR_FUNCTION_STRING
    func(a, true, "a");
}