// 2.5 Retorno, argumentos e parametros de funcoes
/* Na hora da chamada da funcao, caso houver um numero menor de argumentos que o
necessario, deve-se lancar o erro ERR_MISSING_ARGS. */

int a;
bool func(int e, bool ashasduhsd, float z) {
    e = 1;
    func(); // ERR_MISSING_ARGS
}