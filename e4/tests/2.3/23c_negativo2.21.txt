// 2.3 Uso correto de identificadores
/* Caso o identificador dito vetor seja usado como variavel ou 
funcao, deve-se lancar o erro ERR_VECTOR. */

int a[3];
bool func() {
    a[2] = a() + 1; // ERR_VECTOR
}