#ifndef __ast_h__
#define __ast_h__

#define MAX_FILHOS 5

typedef enum TipoValorLexico {
    CARACTER_ESPECIAL = 1,
    OP_COMPOSTO = 2,
    TVL_IDENTIFICADOR = 3,
    LIT_INT = 4,
    LIT_FLOAT = 5,
    LIT_CHAR = 6,
    LIT_STRING = 7,
    LIT_BOOL = 8
} TipoValorLexico;

typedef enum TipoNodo {
    LITERAL,
    FUNCAO,
    COMANDO,
    CHAMADA_FUNCAO,
    ATRIBUICAO,
    ACESSO_ARRAY,
    IDENTIFICADOR,
    OP_BINARIA,
    OP_UNARIA,
    INICIALIZACAO,
    PROGRAMA = 20
} TipoNodo;

typedef struct ValorLexico {
    TipoValorLexico tipo;
    int linha;
    int tipoDeDado;
    union {
        int vInt;
        float vFloat;
        char *vStr;
        char *vChar;
    } valor;
} ValorLexico;

typedef struct Nodo {
    TipoNodo tipo;
    int tipoDeDado; // referencia ao enum TipoDeDado em tabela_simbolos.h
    struct ValorLexico *valorLexico;
    struct Nodo *filhos[MAX_FILHOS];
} Nodo;

void exporta(void *arvore);

void libera(void *arvore);

Nodo* criaNodo(TipoNodo tipoNodo, ValorLexico* valorLexico,  Nodo* filho0, Nodo* filho1, Nodo* filho2, Nodo* filho3, Nodo* filho4);
ValorLexico* criaValorLexico(TipoValorLexico tipo, int linha, char* valor);
char* getNomeTipoNodo(Nodo *arvoreNode);
char* getLiteralLabel(Nodo *arvoreNode);
void exportaEnderecos(Nodo *arvoreNode);
void exportaLabels(Nodo *arvoreNode);


#endif 