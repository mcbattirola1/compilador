%{
#include <stdio.h>
#include <stdlib.h>
#include "ast.h"
#include "tabela_simbolos.h"
#include "gerador_iloc.h"

int yylex(void);
int yyerror (char const *s);
int get_line_number();

extern void *arvore;
%}

%union {
	struct ValorLexico *valorLexico;
	struct Nodo *nodo;
  int tipoDeDado;
}

%start programa

%token TK_PR_INT
%token TK_PR_FLOAT
%token TK_PR_BOOL
%token TK_PR_CHAR
%token TK_PR_STRING
%token<valorLexico> TK_PR_IF
%token TK_PR_THEN
%token<valorLexico> TK_PR_ELSE
%token<valorLexico> TK_PR_WHILE
%token TK_PR_DO
%token<valorLexico> TK_PR_INPUT
%token<valorLexico> TK_PR_OUTPUT
%token<valorLexico> TK_PR_RETURN
%token TK_PR_CONST
%token TK_PR_STATIC
%token TK_PR_FOREACH
%token<valorLexico> TK_PR_FOR
%token TK_PR_SWITCH
%token TK_PR_CASE
%token<valorLexico> TK_PR_BREAK
%token<valorLexico> TK_PR_CONTINUE
%token TK_PR_CLASS
%token TK_PR_PRIVATE
%token TK_PR_PUBLIC
%token TK_PR_PROTECTED
%token TK_PR_END
%token TK_PR_DEFAULT
%token<valorLexico> TK_OC_LE
%token<valorLexico> TK_OC_GE
%token<valorLexico> TK_OC_EQ
%token<valorLexico> TK_OC_NE
%token<valorLexico> TK_OC_AND
%token<valorLexico> TK_OC_OR
%token<valorLexico> TK_OC_SL
%token<valorLexico> TK_OC_SR
%token<valorLexico> TK_LIT_INT
%token<valorLexico> TK_LIT_FLOAT
%token<valorLexico> TK_LIT_FALSE
%token<valorLexico> TK_LIT_TRUE
%token<valorLexico> TK_LIT_CHAR
%token<valorLexico> TK_LIT_STRING
%token<valorLexico> TK_IDENTIFICADOR
%token<valorLexico> '+'
%token<valorLexico> '-'
%token<valorLexico> '*'
%token<valorLexico> '/'
%token<valorLexico> '>'
%token<valorLexico> '<'
%token<valorLexico> '%'
%token<valorLexico> '|'
%token<valorLexico> '&'
%token<valorLexico> '^'
%token<valorLexico> '#'
%token<valorLexico> '?'
%token<valorLexico> '!'
%token TOKEN_ERRO

%type<nodo> id
%type<nodo> programa
%type<nodo> funcao
%type<nodo> bloco_comandos
%type<nodo> lista_comandos
%type<nodo> comando
%type<nodo> atribuicao
%type<nodo> id_array
%type<nodo> expressao
%type<nodo> literal
%type<nodo> literal_numerico
%type<nodo> cabecalho_funcao
%type<nodo> input
%type<nodo> output
%type<nodo> declaracao_variaveis_locais
%type<nodo> variavel_local
%type<nodo> lista_variaveis_locais
%type<nodo> chamada_funcao
%type<nodo> lista_argumentos
%type<nodo> if
%type<nodo> for
%type<nodo> while
%type<nodo> shift
%type<nodo> retorno
%type<nodo> break
%type<nodo> continue
%type<nodo> comando_com_bloco

%type<tipoDeDado> tipo

%left "ternario"
%left "binario"
%left "unario"

%left TK_OC_OR
%left TK_OC_AND
%left '|'
%left '^'
%left TK_OC_SL TK_OC_SR
%left TK_OC_EQ TK_OC_NE
%left '>' TK_OC_LE '<' TK_OC_GE  
%left '+' '-' '!' '#'  '?' '&' '%'  
%left '*' '/'

%%

programa: variavel_global programa { $$ = $2; }
| funcao programa { $1->filhos[0] = $2; $$ = $1; arvore = (void*) $$; }
| /* vazio */ { $$ = NULL; imprimeCodigo(); } ;

tipo: TK_PR_INT { $$ = TIPO_INT; } 
| TK_PR_FLOAT { $$ = TIPO_FLOAT; } 
| TK_PR_BOOL { $$ = TIPO_BOOLEAN; } 
| TK_PR_CHAR { $$ = TIPO_CHAR; } 
| TK_PR_STRING { $$ = TIPO_STRING; } ;

literal: TK_LIT_CHAR { $$ = criaNodo(LITERAL, $1, 0, 0, 0, 0, 0); $$->tipoDeDado = TIPO_CHAR; }
| TK_LIT_FALSE { $$ = criaNodo(LITERAL, $1, 0, 0, 0 ,0 ,0);$$->tipoDeDado = TIPO_BOOLEAN; }
| TK_LIT_FLOAT  { $$ = criaNodo(LITERAL, $1, 0, 0, 0 ,0 ,0); $$->tipoDeDado = TIPO_FLOAT; }
| TK_LIT_INT { $$ = criaNodo(LITERAL, $1, 0, 0, 0 ,0 ,0); $$->tipoDeDado = TIPO_INT; }
| TK_LIT_STRING { $$ = criaNodo(LITERAL, $1, 0, 0, 0, 0, 0); $$->tipoDeDado = TIPO_STRING; }
| TK_LIT_TRUE { $$ = criaNodo(LITERAL, $1, 0, 0, 0 ,0 ,0); $$->tipoDeDado = TIPO_BOOLEAN;} ;

literal_numerico: TK_LIT_FLOAT { $$ = criaNodo(LITERAL, $1, 0, 0,0 ,0 ,0); $$->tipoDeDado = TIPO_FLOAT;}
| TK_LIT_INT { $$ = criaNodo(LITERAL, $1, 0, 0,0 ,0 ,0); $$->tipoDeDado = TIPO_INT;} ;

static_opcional: TK_PR_STATIC | /* vazio */ ;
const_opcional: TK_PR_CONST | /* vazio */ ;

lista_nome_var_global: nome_var_global ',' lista_nome_var_global | nome_var_global;

nome_var_global: id { criaSimboloSemTipo($1->valorLexico, NAT_VARIAVEL, 1); } 
| id '[' TK_LIT_INT ']' { criaSimboloSemTipo($1->valorLexico, NAT_ARRAY, $3->valor.vInt); }
| id '[' '+' TK_LIT_INT ']' { criaSimboloSemTipo($1->valorLexico, NAT_ARRAY, $4->valor.vInt); } ;

variavel_global: static_opcional tipo lista_nome_var_global ';' {
  adicionaTipoASimbolos($2);
};

funcao: cabecalho_funcao bloco_comandos { $$ = criaNodo(FUNCAO, $1->valorLexico, 0, $2, 0, 0, 0); geraFuncao( $1->valorLexico->valor.vStr); };

cabecalho_funcao: static_opcional tipo id '(' lista_parametros ')' {
  $$ = $3;
  criaSimbolo($3->valorLexico, $2, NAT_FUNCAO, 1);
  setTipoDaFuncaoAtual($2, $3->valorLexico);
  } ;
parametro: const_opcional tipo id { criaParametro($3->valorLexico, $2, NAT_VARIAVEL, 1); } ;
lista_parametros: parametro ',' lista_parametros | parametro | /* vazio */ ;

inicio_bloco_comandos: '{' { pushEscopo(); };
fim_bloco_comandos: '}' { popEscopo(); };
bloco_comandos: inicio_bloco_comandos lista_comandos fim_bloco_comandos { $$ = $2; } | '{' '}' { $$ = NULL; } ;

comando: declaracao_variaveis_locais { $$ = $1; } 
| atribuicao { $$ = $1; }
| input { $$ = $1; }
| output { $$ = $1; }
| chamada_funcao  { $$ = $1; }
| shift { $$ = $1; }
| retorno { $$ = $1; }
| break { $$ = $1; }
| continue { $$ = $1; }
| bloco_comandos { $$ = $1; };

comando_com_bloco: if { $$ = $1; }
| for { $$ = $1; }
| while { $$ = $1; } ;

break: TK_PR_BREAK { $$ = criaNodo(COMANDO, $1, 0, 0, 0, 0, 0); }
continue: TK_PR_CONTINUE { $$ = criaNodo(COMANDO, $1, 0, 0, 0, 0, 0); }

lista_comandos: comando ';' lista_comandos { 
    // TODO: refatorar
    if ($$ == NULL) {
      $$ = $3;
    } else {
      Nodo* aux = $$;
      while(aux->filhos[0] != NULL) {
        aux = aux->filhos[0];
      }
      aux->filhos[0] = $3;
      $$ = $1;
    }
  }
| comando ';' { $$ = $1; } 
| comando_com_bloco { $$ = $1; }
| comando_com_bloco lista_comandos {
  if ($$ == NULL) {
      $$ = $2;
    } else {
      Nodo* aux = $$;
      while(aux->filhos[0] != NULL) {
        aux = aux->filhos[0];
      }
      aux->filhos[0] = $2;
      $$ = $1;
    }
 } ;

variavel_local: id { 
    $$ = NULL; 
    criaInicializacao(0, 0);
    criaSimboloSemTipo($1->valorLexico, NAT_VARIAVEL, 1); 
  }
| id TK_OC_LE literal { 
  criaInicializacao($3->tipoDeDado, $3->valorLexico);
  $$ = criaNodo(INICIALIZACAO, $2, 0, $1, $3, 0, 0); 
  criaSimboloSemTipo($1->valorLexico, NAT_VARIAVEL, 1);
}
| id TK_OC_LE id { 
  validaUsoIdentificador($3->valorLexico);
  criaInicializacao(getTipo($3->valorLexico), $3->valorLexico);
  $$ = criaNodo(INICIALIZACAO, $2, 0, $1, $3, 0, 0); 
  criaSimboloSemTipo($1->valorLexico, NAT_VARIAVEL, 1); 
};

lista_variaveis_locais: variavel_local ',' lista_variaveis_locais {
  if($1 == NULL) {
    $$ = $3;
  } else {
    $1->filhos[0] = $3; $$ = $1; 
  }
}
| variavel_local { $$ = $1; } ;

declaracao_variaveis_locais: static_opcional const_opcional tipo lista_variaveis_locais { 
    adicionaTipoASimbolos($3);
    $$ = $4;
  };

id: TK_IDENTIFICADOR { $$ = criaNodo(IDENTIFICADOR, $1, 0, 0, 0, 0, 0); }; 

expressao: id '[' expressao ']' { 
    $$ = criaNodo(ACESSO_ARRAY, 0, 0, $1, $3, 0, 0); 
    $$->tipoDeDado = getTipo($1->valorLexico);
    validaUsoIdentificador($1->valorLexico);
    validaSeEhArray($1->valorLexico);
    // (?) validar se expressao eh tipo int
  }
  | id { 
    $$ = $1;
    validaUsoIdentificador($1->valorLexico);
    $$->tipoDeDado = getTipo($1->valorLexico);
    validaSeEhVariavel($1->valorLexico);
  }
  | TK_LIT_TRUE { 
    $$ = criaNodo(LITERAL, $1, 0, 0, 0, 0, 0); 
    $$->tipoDeDado = TIPO_BOOLEAN;
  }
  | TK_LIT_FALSE { $$ = criaNodo(LITERAL, $1, 0, 0, 0, 0, 0); $$->tipoDeDado = TIPO_BOOLEAN; }
  | literal_numerico { 
      $$ = $1; 
      // ja tem tipo de dado, vem do literal_numerico
    }
  | chamada_funcao { $$ = $1; }
  | '(' expressao ')' { $$ = $2; }
  | '-' expressao { $$ = criaNodo(OP_UNARIA, $1, 0, $2, 0, 0, 0); $$->tipoDeDado = $2->tipoDeDado; }
  | '+' expressao { $$ = criaNodo(OP_UNARIA, $1, 0, $2, 0, 0, 0); $$->tipoDeDado = $2->tipoDeDado; }
  | '!' expressao { $$ = criaNodo(OP_UNARIA, $1, 0, $2, 0, 0, 0); $$->tipoDeDado = $2->tipoDeDado; }
  | '&' expressao { $$ = criaNodo(OP_UNARIA, $1, 0, $2, 0, 0, 0); $$->tipoDeDado = $2->tipoDeDado; }
  | '*' expressao { $$ = criaNodo(OP_UNARIA, $1, 0, $2, 0, 0, 0); $$->tipoDeDado = $2->tipoDeDado; }
  | '?' expressao { $$ = criaNodo(OP_UNARIA, $1, 0, $2, 0, 0, 0); $$->tipoDeDado = $2->tipoDeDado; }
  | '#' expressao { $$ = criaNodo(OP_UNARIA, $1, 0, $2, 0, 0, 0); $$->tipoDeDado = $2->tipoDeDado; } 
  | expressao '+' expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0); $$->tipoDeDado = infereTipoOpBinaria($1, $3); }
  | expressao '-' expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0); $$->tipoDeDado = infereTipoOpBinaria($1, $3); }
  | expressao '*' expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0); $$->tipoDeDado = infereTipoOpBinaria($1, $3); }
  | expressao '/' expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0); $$->tipoDeDado = infereTipoOpBinaria($1, $3); }
  | expressao '%' expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0); $$->tipoDeDado = infereTipoOpBinaria($1, $3); }
  | expressao '|' expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0); $$->tipoDeDado = infereTipoOpBinaria($1, $3); }
  | expressao '&' expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0); $$->tipoDeDado = infereTipoOpBinaria($1, $3); }
  | expressao '^' expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0); $$->tipoDeDado = infereTipoOpBinaria($1, $3); }
  | expressao '>' expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0); $$->tipoDeDado = infereTipoOpBinaria($1, $3); }
  | expressao '<' expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0); $$->tipoDeDado = infereTipoOpBinaria($1, $3); }
  | expressao TK_OC_LE expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0); $$->tipoDeDado = infereTipoOpBinaria($1, $3); }
  | expressao TK_OC_GE expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0); $$->tipoDeDado = infereTipoOpBinaria($1, $3); }
  | expressao TK_OC_EQ expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0); $$->tipoDeDado = infereTipoOpBinaria($1, $3); }
  | expressao TK_OC_NE expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0); $$->tipoDeDado = infereTipoOpBinaria($1, $3); }
  | expressao TK_OC_AND expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0); $$->tipoDeDado = infereTipoOpBinaria($1, $3); }
  | expressao TK_OC_OR expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0); $$->tipoDeDado = infereTipoOpBinaria($1, $3); }
  | expressao '?' expressao ':' expressao %prec "ternario" {
    // validar se $1 eh bool
    // inferir o tipo a partir de $ 3 e $5;
  };

id_array:  id '[' expressao ']' {
  validaUsoIdentificador($1->valorLexico);
  validaSeEhArray($1->valorLexico);
  $$ = criaNodo(ACESSO_ARRAY, 0, 0, $1, $3, 0, 0); 
} ;

atribuicao: id '=' expressao { 
    validaUsoIdentificador($1->valorLexico);
    validaSeEhVariavel($1->valorLexico);
    validaAtribuicao($1->valorLexico, $3);
    $$ = criaNodo(ATRIBUICAO, 0, 0, $1, $3, 0, 0); 
  }
| id '=' TK_LIT_STRING { 
    validaUsoIdentificador($1->valorLexico);
    validaSeEhVariavel($1->valorLexico);
    validaAtribuicaoLitString($1->valorLexico, $3);
    Nodo* nodoString = criaNodo(LITERAL, $3, 0, 0, 0, 0, 0);
    $$ = criaNodo(ATRIBUICAO, 0, 0, $1, nodoString, 0, 0); 
  }
| id '=' TK_LIT_CHAR { 
    validaUsoIdentificador($1->valorLexico);
    validaSeEhVariavel($1->valorLexico);
    Nodo* nodoChar = criaNodo(LITERAL, $3, 0, 0, 0, 0, 0);
    validaAtribuicao($1->valorLexico, nodoChar);
    $$ = criaNodo(ATRIBUICAO, 0, 0, $1, nodoChar, 0, 0); 
  }
| id_array '=' expressao {  
  validaAtribuicao($1->filhos[1]->valorLexico, $3);
  $$ = criaNodo(ATRIBUICAO, 0, 0, $1, $3, 0, 0); 
}
| id_array '=' TK_LIT_CHAR {
  // TODO: fix this
  // Nodo* nodoChar = criaNodo(LITERAL, $3, 0, 0, 0, 0, 0);
  // validaAtribuicao($1->valorLexico, nodoChar);
  // $$ = criaNodo(ATRIBUICAO, 0, 0, $1, nodoChar, 0, 0); 
  }
;

input: TK_PR_INPUT id {
  validaTipoInput($2->valorLexico);
  $$ = criaNodo(COMANDO, $1, 0, $2, 0, 0, 0);
};

output: TK_PR_OUTPUT id { 
  validaTipoOutput($2->valorLexico);
  $$ = criaNodo(COMANDO, $1, 0, $2, 0, 0, 0);
} 
| TK_PR_OUTPUT literal { validaTipoOutputLiteral($2->tipoDeDado, $2->valorLexico->linha); $$ = criaNodo(COMANDO, $1, 0, $2, 0, 0, 0); }
| TK_PR_OUTPUT '-' literal_numerico { Nodo* novoNodo = criaNodo(OP_UNARIA, $2, 0, $3, 0, 0, 0); $$ = criaNodo(COMANDO, $1, 0, novoNodo, 0, 0, 0);} ;

chamada_funcao: id '(' lista_argumentos ')' { 
    validaUsoIdentificador($1->valorLexico);
    validaSeEhFuncao($1->valorLexico);
    validaArgumentos($1->valorLexico);
    $$ = criaNodo(CHAMADA_FUNCAO, $1->valorLexico, 0, $3, 0, 0, 0);
    $$->tipoDeDado = getTipo($1->valorLexico);
    geraChamadaFuncao();
  };
lista_argumentos: expressao ',' lista_argumentos { $1->filhos[0] = $3; $$ = $1; adicionaArgumentoNaLista($1->tipoDeDado); }
| expressao { 
  $$ = $1; 
  adicionaArgumentoNaLista($1->tipoDeDado);
}
| { $$ = NULL; } /* vazio */;

if: TK_PR_IF '(' expressao ')' bloco_comandos { $$ = criaNodo(COMANDO, $1, 0, $3, $5, 0, 0); }
| TK_PR_IF '(' expressao ')' bloco_comandos TK_PR_ELSE bloco_comandos { 
    Nodo* nodoElse = criaNodo(COMANDO, $6, 0, $7, 0, 0, 0);
    $$ = criaNodo(COMANDO, $1, 0, $3, $5, $7, 0); 
  } ;

for: TK_PR_FOR '(' atribuicao ':' expressao ':' atribuicao ')' bloco_comandos  {
  $$ = criaNodo(COMANDO, $1, 0, $3, $5, $7, $9);
};

while: TK_PR_WHILE '(' expressao ')' TK_PR_DO bloco_comandos {
  $$ = criaNodo(COMANDO, $1, 0, $3, $6, 0, 0);
};

shift: id '[' expressao ']' TK_OC_SL TK_LIT_INT {
  Nodo* nodoAcesso = criaNodo(ACESSO_ARRAY, 0, 0, $1, $3, 0, 0);
  Nodo* nodoLitInt = criaNodo(LITERAL, $6, 0, 0,0 ,0 ,0);
  $$ = criaNodo(COMANDO, $5, 0, nodoAcesso, nodoLitInt, 0, 0);
  validaQtdShift($1->valorLexico, $6->valor.vInt);
 }
| id TK_OC_SL TK_LIT_INT {
  Nodo* nodoLitInt = criaNodo(LITERAL, $3, 0, 0,0 ,0 ,0);
  $$ = criaNodo(COMANDO, $2, 0, $1, nodoLitInt, 0, 0);
  validaQtdShift($1->valorLexico, $3->valor.vInt);
}
| id '[' expressao ']' TK_OC_SR TK_LIT_INT {
  Nodo* nodoAcesso = criaNodo(ACESSO_ARRAY, 0, 0, $1, $3, 0, 0);
  Nodo* nodoLitInt = criaNodo(LITERAL, $6, 0, 0,0 ,0 ,0);
  $$ = criaNodo(COMANDO, $5, 0, nodoAcesso, nodoLitInt, 0, 0);
  validaQtdShift($1->valorLexico, $6->valor.vInt);
}
| id TK_OC_SR TK_LIT_INT {
  Nodo* nodoLitInt = criaNodo(LITERAL, $3, 0, 0,0 ,0 ,0);
  $$ = criaNodo(COMANDO, $2, 0, $1, nodoLitInt, 0, 0);
  validaQtdShift($1->valorLexico, $3->valor.vInt);
};

retorno: TK_PR_RETURN expressao { 
    $$ = criaNodo(COMANDO, $1, 0, $2, 0, 0, 0); 
    validaTipoRetorno($2->tipoDeDado, $1->linha);
    geraSequenciaDeRetorno(0);
  } 
  | TK_PR_RETURN TK_LIT_CHAR {
    Nodo* nodo = criaNodo(LITERAL, $2, 0, 0, 0, 0, 0); 
    $$ = criaNodo(COMANDO, $1, 0, nodo, 0, 0, 0); 
    validaTipoRetorno(TIPO_CHAR, $1->linha);
  };

%%

int yyerror(char const *s) {
    printf("error on line %d: %s\n", get_line_number(), s);
    return 1;
}
