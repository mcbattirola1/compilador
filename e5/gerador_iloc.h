#include "ast.h"

typedef enum Escopo {
    ESCOPO_GLOBAL = 1,
    ESCOPO_LOCAL = 2,
} Escopo;

void addToBuffer(char* text);
char* getRegDoEscopo(Escopo escopo);
void geraVariavelLocalComInicializacao(int deslocamento, int valor);
void geraVariavelLocalSemInicializacao();
void geraVariavelGlobalSemInicializacao();
void geraAtribuicaoLiteral(int deslocamento, int valor, Escopo escopoDaVariavel);
char* geraFuncao(char* nomeFuncao);
void geraSequenciaDeRetorno(int qtdParametros);
void esvaziaBufferIntermediario();
void imprimeCodigo();
char* geraRotulo();
void geraChamadaFuncao();
char *str_replace(char *orig, char *rep, char *with);