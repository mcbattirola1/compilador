#include <stdio.h>
#include <stdlib.h>
#include <string.h> 
#include "tabela_simbolos.h"
#include "gerador_iloc.h"
#include "ast.h"
#include "errors.h"

PilhaDeEscopos* pilha;
Simbolo* simbolosDeTipoDesconhecido[MAX_TIPOS_DESCONHECIDOS];
int indiceUltimoDesconhecido = 0;

int currentStringsIndexTS = 0;
char* stringsTS[30000];

Simbolo* parametrosAguardandoEscopo[MAX_ARGS];
int indiceUltimoParametro = -1;

int auxSimbolo = -1;
int auxEscopo = -1;

// funcao atual eh a funcao sendo analisada no momento
// -1 representa o escopo global
int tipoDaFuncaoAtual = -1;
char* chaveDaFuncaoAtual;

int argumentosLidos[MAX_ARGS];
int indiceUltimoArgumento = -1;

int atribuicoesLidas[100];
ValorLexico* valoresAtribuicoes[100];
int indiceUltimaAtribuicao = -1;

Simbolo* funcaoSendoLida;

int escopoDoUltimoEncontradoPeloGetSimboloDaPilha = 0;

void criaSimbolo(ValorLexico *valorLexico, int tipo, Natureza natureza, int qtdItems) {
    // cria struct
    Simbolo *item = calloc(1, sizeof(Simbolo));
    item->valorLexico = valorLexico;
    item->natureza = natureza;
    item->linha = valorLexico->linha;
    item->chave = getChave(valorLexico);
    item->qtdItems = qtdItems;
    item->tipo = tipo;
    item->tamanho = getTamanhoPorTipo(tipo) * item->qtdItems;

    if (chaveJaNoEscopoAtual(item->chave)) {
        TerminaComErro(ERR_DECLARED, item->chave, item->linha);
    }

    if(item->natureza == NAT_FUNCAO) {
        // Uma funcao nao pode ser do tipo string
        if (tipo == TIPO_STRING) {
            TerminaComErro(ERR_FUNCTION_STRING, item->chave, item->linha);
        }

        funcaoSendoLida = item;

        // item->label = geraFuncao(valorLexico->valor.vStr);
    }

    adicionaSimboloAoEscopoAtual(item);
}

void criaSimboloSemTipo(ValorLexico *valorLexico, Natureza natureza, int qtdItems) {
    // cria struct
    Simbolo *item = calloc(1, sizeof(Simbolo));
    item->valorLexico = valorLexico;
    item->natureza = natureza;
    item->linha = valorLexico->linha;
    item->chave = getChave(valorLexico);
    item->qtdItems = qtdItems;

    if (chaveJaNoEscopoAtual(item->chave)) {
        printf("acho q entrou aqui\n");
        TerminaComErro(ERR_DECLARED, item->chave, item->linha);
    }

    // TODO: demais checagens de erro

    simbolosDeTipoDesconhecido[indiceUltimoDesconhecido] = item;
    indiceUltimoDesconhecido++;
}

// adiciona o tipo a um array de tipos de inicializacoes.
// o tipo NULL significa uma inicializacao sem atribuicao
void criaInicializacao(int tipo, ValorLexico* valor) {
    // joga pra um array o tipo do valorLexico
    indiceUltimaAtribuicao++;
    atribuicoesLidas[indiceUltimaAtribuicao] = tipo;
    valoresAtribuicoes[indiceUltimaAtribuicao] = valor;
}

// adicionaTipoASimbolos adiciona os tipos dos simbolos encontrados
// e joga eles para o escopo atual
void adicionaTipoASimbolos(int tipo) {
    // nesse ponto, foi achado um tipo
    // e criamos uma lista de identificadores sem tipo
    // agora, vamos colocar o tipo encontrado nesses identificadores
    for (int i = 0; i <= indiceUltimoDesconhecido; i++) {
        Simbolo* item = simbolosDeTipoDesconhecido[i];
        if (item != NULL) {
            // printf("adicionaTipoASimbolos: item chegou NULL\n");
            item->tipo = tipo;
            item->tamanho = getTamanhoPorTipo(tipo) * item->qtdItems;


            // Um array nao pode ser do tipo string
            if(item->natureza == NAT_ARRAY && tipo == TIPO_STRING) {
                TerminaComErro(ERR_STRING_VECTOR, item->chave, item->linha);
            }
            adicionaSimboloAoEscopoAtual(item);

            if (item->tipo == TIPO_INT) {

                Escopo escopo = ESCOPO_LOCAL;
                if (getPilhaGlobal()->indiceUltimoEscopo == 0) {
                    escopo = ESCOPO_GLOBAL;
                }

                if (escopo != ESCOPO_GLOBAL) {
                    if (valoresAtribuicoes[i] != NULL) {
                        geraVariavelLocalComInicializacao(getEscopoAtual()->deslocamento, valoresAtribuicoes[i]->valor.vInt);
                    } else {
                        geraVariavelLocalSemInicializacao();
                    }
                } else {
                    geraVariavelGlobalSemInicializacao();
                }
                
            }
        }

    }

    for (int i = 0; i <= indiceUltimaAtribuicao; i++)
    {
        // o tipo NULL significa uma inicializacao sem atribuicao
        int atribuicao = atribuicoesLidas[i];
        if (atribuicao != 0) {
            if (!tiposSaoCompativeis(tipo, atribuicoesLidas[i])) {
                TerminaComErro(ERR_WRONG_TYPE, "unknown", 0);
            }

            // se a atribuicao foi uma string, contamos o numero de caracteres e adicionamos ao qtdItems do simbolo
            if (tipo == TIPO_STRING) {
                simbolosDeTipoDesconhecido[i]->qtdItems = strlen(valoresAtribuicoes[i]->valor.vStr);
                simbolosDeTipoDesconhecido[i]->tamanho = getTamanhoPorTipo(TIPO_STRING) * simbolosDeTipoDesconhecido[i]->qtdItems;
            }

        }
    }

    indiceUltimoDesconhecido = 0;
    indiceUltimaAtribuicao = -1;
    
}

void adicionaArgumentoNaLista(int tipo) {
    indiceUltimoArgumento++;
    argumentosLidos[indiceUltimoArgumento] = tipo;
}

// 
void criaParametro(ValorLexico *valorLexico, int tipo, Natureza natureza, int qtdItems) {
    // cria struct
    Simbolo *item = calloc(1, sizeof(Simbolo));
    item->valorLexico = valorLexico;
    item->natureza = natureza;
    item->linha = valorLexico->linha;
    item->chave = getChave(valorLexico);
    item->qtdItems = qtdItems;
    item->tipo = tipo;
    item->tamanho = getTamanhoPorTipo(tipo) * item->qtdItems;

    // Um parametro nao pode ser do tipo string
    if(tipo == TIPO_STRING) {
        TerminaComErro(ERR_FUNCTION_STRING, item->chave, item->linha);
    }

    indiceUltimoParametro++;
    parametrosAguardandoEscopo[indiceUltimoParametro] = item;
}



TabelaDeSimbolos* getEscopoAtual() {
    return getEscopo(getPilhaGlobal()->indiceUltimoEscopo);
}

TabelaDeSimbolos* getEscopo(int indice) {
    return getPilhaGlobal()->escopo[indice];
}

// singleton
PilhaDeEscopos* getPilhaGlobal() {
    if (pilha == NULL) {
        pilha = calloc(1, sizeof(PilhaDeEscopos));
        
        TabelaDeSimbolos* escopo = calloc(1, sizeof(TabelaDeSimbolos));
        escopo->indiceUltimoSimbolo = -1;
        pilha->escopo[0] = escopo;
        pilha->indiceUltimoEscopo = 0;
    }

    return pilha;
}

// chaveJaNaPilha retorna 0  se a chave ja esta na pilha e 0 caso contrario
// alem disso, seta as variaveis auxSimbolo e auxEscopo
// que representam o indice do escopo em que a variavel foi encontrada
// e o indice da chave nesse escopo
int chaveJaNaPilha(char* chave) {
    PilhaDeEscopos *pilha = getPilhaGlobal();
    for (int i = pilha->indiceUltimoEscopo; i >= 0 ; i--)
    {
        TabelaDeSimbolos* escopo = getEscopo(i);

        if (chaveJaNoEscopo(chave, escopo)) {
            auxEscopo = i;
            return 1;
        }
    }

    return 0;    
}

// chaveJaNoEscopo retorna 0  se a chave ja esta no escopo e 0 caso contrario
// alem disso, seta as variaveis auxSimbolo
// e o indice da chave nesse escopo
int chaveJaNoEscopo(char* chave, TabelaDeSimbolos* escopo) {
    for (int i = 0; i <= escopo->indiceUltimoSimbolo; i++)
    {
        if (strcmp(escopo->simbolos[i]->chave, chave) == 0) {
            auxSimbolo = i;
            return 1;
        }
    }
    
    return 0;
}

int chaveJaNoEscopoAtual(char* chave) {
    return chaveJaNoEscopo(chave, getEscopoAtual());
}

char* getChave(ValorLexico *valorLexico) {
    switch (valorLexico->tipo) {
    case TVL_IDENTIFICADOR:
        return valorLexico->valor.vStr;
    case LIT_INT: ;
        char str[100];
        sprintf(str, "%d", valorLexico->valor.vInt);
        char* ret = strdup(str);
        currentStringsIndexTS++;
        return ret; // aqui
    case LIT_FLOAT: ;
        char array[100];
        sprintf(array, "%f", valorLexico->valor.vFloat);
        char* retF = strdup(array);
        currentStringsIndexTS++;
        return retF; //aqui
    case LIT_CHAR:
        return valorLexico->valor.vChar;
    case LIT_STRING:
        return valorLexico->valor.vStr;
    case LIT_BOOL: 
        if (valorLexico->valor.vInt == 1) {
            return "true";
        }
        return "false";
    }
}

void setTipoDaFuncaoAtual(int tipo, ValorLexico* valorLexico) {
    tipoDaFuncaoAtual = tipo;
    chaveDaFuncaoAtual = strdup(getChave(valorLexico));
}

void TerminaComErro(int tipoErro, char* chave, int linha) {
    switch(tipoErro) {
        case ERR_DECLARED:
            printf("Error on line %d: variable '%s' already declared.\n", linha, chave);
            break;
        case ERR_UNDECLARED:
            printf("Error on line %d: variable '%s' is not declared.\n", linha, chave);
            break;
        case ERR_VARIABLE:
            printf("Error on line %d: '%s' used as vector or function, but it is a variable.\n", linha, chave);
            break;
        case ERR_VECTOR:
            printf("Error on line %d: '%s' is a vector, but its used as variable or function.\n", linha, chave);
            break;
        case ERR_FUNCTION:
            printf("Error on line %d: '%s' is a function, but is used as a variable or vector.\n", linha, chave);
            break;
        case ERR_MISSING_ARGS:
            printf("Error on line %d: function '%s' is missing arguments.\n", linha, chave);
            break;
        case ERR_EXCESS_ARGS:
            printf("Error on line %d: function '%s' called with too many arguments.\n", linha, chave);
            break;
        case ERR_WRONG_TYPE_ARGS:
            printf("Error on line %d: function '%s' called with wrong argument type.\n", linha, chave);
            break;
        case ERR_STRING_VECTOR:
            printf("Error on line %d: string vector is not supported.\n", linha);
            break;
        case ERR_FUNCTION_STRING:
            printf("Error on line %d: string is not valid for function type or parameter.\n", linha);
            break;
        case ERR_WRONG_PAR_INPUT:
            printf("Error on line %d: command 'input' requires a parameter of type int or float\n", linha);
            break;
        case ERR_WRONG_PAR_OUTPUT:
            if (chave == NULL) {
                printf("Error on line %d: command 'output' requires a parameter of type int or float\n", linha);
            } else {
                printf("Error on line %d: command 'output' requires a parameter of type int or float, but got %s, which is not.\n", linha, chave);
            }
            
            break;
        case ERR_WRONG_TYPE:
            printf("Error on line %d: cant set variable '%s' due to wrong type.\n", linha, chave);
            break;
        case ERR_STRING_TO_X:
            printf("Error on line %d: Can't implicitly convert string to another type.\n", linha);
            break;
        case ERR_CHAR_TO_X:
            printf("Error on line %d: Can't implicitly convert char to another type.\n", linha);
            break;
        case ERR_WRONG_PAR_RETURN:
            printf("Error on line %d: Wrong return type for function '%s'.\n", linha, chave);
            break;
        case ERR_WRONG_PAR_SHIFT:
            printf("Error on line %d: Shift on variable '%s' can't be larger than 16.\n", linha, chave);
            break;
        case ERR_STRING_MAX:
            printf("Error on line %d: Max size exceeded on string '%s'.\n", linha, chave);
            break;
        default:
            printf("Unknown error on line %d.\n", linha);
            break;
    }
    exit(tipoErro);
}

void pushEscopo() {
    // descarrega o buffer do gerador iloc
    esvaziaBufferIntermediario();


    TabelaDeSimbolos* escopo = calloc(1, sizeof(TabelaDeSimbolos));
    escopo->indiceUltimoSimbolo = -1;
    getPilhaGlobal()->indiceUltimoEscopo++;
    getPilhaGlobal()->escopo[getPilhaGlobal()->indiceUltimoEscopo] = escopo;

    // se funcao
    // adiciona parametros no escopo 

    // adicionamos os parametros ao escopo atual
    // adicionamos os parametros ao Simbolo da funcao
    for (int i = 0; i <= indiceUltimoParametro; i++)
    {
        adicionaSimboloAoEscopoAtual(parametrosAguardandoEscopo[i]);
        funcaoSendoLida->tiposArgumentos[i] = parametrosAguardandoEscopo[i]->tipo;        
    }

    funcaoSendoLida->qtdItems = indiceUltimoParametro + 1;

    // printf("%d parametros na funcao %s\n", funcaoSendoLida->qtdItems, funcaoSendoLida->chave);

    indiceUltimoParametro = -1;
    
}

void popEscopo() {
    getPilhaGlobal()->indiceUltimoEscopo--;
    // free?
}

void adicionaSimboloAoEscopoAtual(Simbolo* simbolo) {
    TabelaDeSimbolos* escopoAtual = getEscopoAtual();
    simbolo->deslocamento = escopoAtual->deslocamento;

    escopoAtual->indiceUltimoSimbolo++;
    escopoAtual->deslocamento += simbolo->tamanho;
    
    escopoAtual->simbolos[escopoAtual->indiceUltimoSimbolo] = simbolo;
}

// retorna o tamanho em bytes de um elemento do tipo enviado
// em  caso de arrays, deve ser multiplicado pela quantidade de items
int getTamanhoPorTipo(int tipo) {
    switch (tipo) {
        case TIPO_INT:
            return 4;
        case TIPO_FLOAT: 
            return 8;
        default:
            return 1;
        break;
    }
    return 1;
}

Simbolo* getSimboloDaPilha(char* chave) {
    PilhaDeEscopos *pilha = getPilhaGlobal();
    for (int i = pilha->indiceUltimoEscopo; i >= 0 ; i--)
    {
        TabelaDeSimbolos* escopo = getEscopo(i);

        Simbolo* item = getSimboloDoEscopo(chave, escopo);
        if (item != NULL) {
            escopoDoUltimoEncontradoPeloGetSimboloDaPilha = i;
            return item;
        }
    }

    return NULL;
}

Simbolo* getSimboloDoEscopo(char* chave, TabelaDeSimbolos* escopo) {
    for (int i = 0; i <= escopo->indiceUltimoSimbolo; i++)
    {
        if (strcmp(escopo->simbolos[i]->chave, chave) == 0) {
            return escopo->simbolos[i];
        }
    }
    
    return NULL;
}

// recebe dois tipos e retorna o tipo inferido
int infereTipoOpBinaria(Nodo* n1, Nodo* n2) {
    int t1 = n1->tipoDeDado;
    int t2 = n2->tipoDeDado;

    if (t1 == t2) {
        return t1;
    }

    if (t1 == TIPO_STRING || t2 == TIPO_STRING) {
        TerminaComErro(ERR_STRING_TO_X, "", n1->valorLexico->linha);
    }

    if (t1 == TIPO_CHAR || t2 == TIPO_CHAR) {
        TerminaComErro(ERR_CHAR_TO_X, "", n1->valorLexico->linha);
    }

    if (t1 == TIPO_FLOAT || t2 == TIPO_FLOAT) {
        return TIPO_FLOAT;
    }

    if (t1 == TIPO_BOOLEAN && t2 == TIPO_BOOLEAN) {
        return TIPO_BOOLEAN;
    }

    return TIPO_INT;
}

int getTipo(ValorLexico* valorLexico) {
    char* chave = getChave(valorLexico);

    Simbolo* simbolo = getSimboloDaPilha(chave);
    return simbolo->tipo;
}

// tiposSaoCompativeis retorna 1 se o tipo esperado eh igual ou passivel de coercao
// em relacao ao tipo recebido
int tiposSaoCompativeis(int tipoEsperado, int tipoRecebido) {
    if (tipoEsperado == tipoRecebido) {
        return 1;
    }

    switch(tipoRecebido) {
        case TIPO_INT:
            return tipoEsperado == TIPO_FLOAT || tipoEsperado == TIPO_BOOLEAN;
        case TIPO_FLOAT:
            return tipoEsperado == TIPO_INT || tipoEsperado == TIPO_BOOLEAN;
        case TIPO_BOOLEAN:
            return tipoEsperado == TIPO_INT || tipoEsperado == TIPO_FLOAT;
        case TIPO_CHAR:
            return 0;
        case TIPO_STRING:
            return 0;
    }

    return 0;
}

void validaUsoIdentificador(ValorLexico* valorLexico) {
    char* chave = getChave(valorLexico);

    // valida chave ja na pilha
    if (!chaveJaNaPilha(chave)) {
        TerminaComErro(ERR_UNDECLARED, chave, valorLexico->linha);
    }
}

void validaAtribuicao(ValorLexico* valorLexico, Nodo* nodoExpressao) {
    int tipoExpressao = nodoExpressao->tipoDeDado;
    // nao pode atribuir para funcoes
    char* chave = getChave(valorLexico);
    Simbolo* item = getSimboloDaPilha(chave);

    if (item->natureza == NAT_FUNCAO) {
        TerminaComErro(ERR_FUNCTION, chave, valorLexico->linha);
    }

    // nao pode atribuir de um tipo diferente do identificador
    if (!tiposSaoCompativeis(item->tipo, tipoExpressao)) {
        if (item->natureza == NAT_ARRAY) {
            printf("Error on line %d: can't set vector '%s' due to wrong type.\n", valorLexico->linha, chave);
            exit(ERR_WRONG_TYPE);
        }
        TerminaComErro(ERR_WRONG_TYPE, chave, valorLexico->linha);
    }
    
    // validar, em caso de string, se a qtdItens ta maior q o length passado
    if (item->tipo == TIPO_STRING) {
        // se for literal, usa o valor
            // podemos ter literal string em atribuicao? no codigo atual nao.

        // se for id, acha o valor na pilha
        if (nodoExpressao->tipo == IDENTIFICADOR) {
            char* chaveExpressao = getChave(nodoExpressao->valorLexico);
            Simbolo* simboloExpressao = getSimboloDaPilha(chaveExpressao);

            if (simboloExpressao->qtdItems > item->qtdItems) {
                printf("Error on line %d: trying to set string variable '%s' of size %d with a string of size %d.\n",
                    nodoExpressao->valorLexico->linha, item->chave, item->qtdItems, simboloExpressao->qtdItems
                );
                exit(ERR_STRING_MAX);
            }
        }
    }

    if (item->tipo == TIPO_INT) {
        Escopo escopo = ESCOPO_LOCAL;

        char* chaveExpressao = getChave(nodoExpressao->valorLexico);
        Simbolo* itemExpressao = getSimboloDaPilha(chaveExpressao);

        if (escopoDoUltimoEncontradoPeloGetSimboloDaPilha == 0) {
            escopo = ESCOPO_GLOBAL;
        }
        geraAtribuicaoLiteral(item->deslocamento, nodoExpressao->valorLexico->valor.vInt, escopo);
    }
}

void validaAtribuicaoLitString(ValorLexico* valorLexico, ValorLexico* valorLexicoString) {
    int tipoExpressao = TIPO_STRING;
    // nao pode atribuir para funcoes
    char* chave = getChave(valorLexico);
    Simbolo* item = getSimboloDaPilha(chave);

    if (item->natureza == NAT_FUNCAO) {
        TerminaComErro(ERR_FUNCTION, chave, valorLexico->linha);
    }

    // nao pode atribuir de um tipo diferente do identificador
    if (!tiposSaoCompativeis(item->tipo, tipoExpressao)) {
        if (item->natureza == NAT_ARRAY) {
            printf("Error on line %d: can't set vector '%s' due to wrong type.\n", valorLexico->linha, chave);
            exit(ERR_WRONG_TYPE);
        }
        TerminaComErro(ERR_WRONG_TYPE, chave, valorLexico->linha);
    }
    
    // validar, em caso de string, se a qtdItens ta maior q o length passado
    if (strlen(valorLexicoString->valor.vStr) > item->qtdItems) {
        printf("Error on line %d: trying to set string variable '%s' of size %d with a string of size %d.\n",
            valorLexicoString->linha, item->chave, item->qtdItems, (int) strlen(valorLexicoString->valor.vStr)
        );
        exit(ERR_STRING_MAX);
    }
}

void validaSeEhArray(ValorLexico* valorLexico) {
    // essa chave ta
    // no escopo auxEscopo
    // e no simbolo auxSimbolo
    Simbolo* simbolo = getPilhaGlobal()->escopo[auxEscopo]->simbolos[auxSimbolo];
    if (simbolo->natureza != NAT_ARRAY) {
        if (simbolo->natureza == NAT_VARIAVEL) {
            TerminaComErro(ERR_VARIABLE, getChave(valorLexico), valorLexico->linha);        
        } else {
            TerminaComErro(ERR_FUNCTION, getChave(valorLexico), valorLexico->linha);        
        }
    }
}

void validaSeEhVariavel(ValorLexico* valorLexico) {
    // essa chave ta
    // no escopo auxEscopo
    // e no simbolo auxSimbolo
    Simbolo* simbolo = getPilhaGlobal()->escopo[auxEscopo]->simbolos[auxSimbolo];
    if (simbolo->natureza != NAT_VARIAVEL) {
        if (simbolo->natureza == NAT_ARRAY) {
            TerminaComErro(ERR_VECTOR, getChave(valorLexico), valorLexico->linha);        
        } else {
            TerminaComErro(ERR_FUNCTION, getChave(valorLexico), valorLexico->linha);        
        }
    }
}

void validaSeEhFuncao(ValorLexico* valorLexico) {
    // essa chave ta
    // no escopo auxEscopo
    // e no simbolo auxSimbolo
    Simbolo* simbolo = getPilhaGlobal()->escopo[auxEscopo]->simbolos[auxSimbolo];
    if (simbolo->natureza != NAT_FUNCAO) {
        if(simbolo->natureza == NAT_VARIAVEL) {
            TerminaComErro(ERR_VARIABLE, getChave(valorLexico), valorLexico->linha);
        } else {
            TerminaComErro(ERR_VECTOR, getChave(valorLexico), valorLexico->linha);
        }
    }
}

void validaArgumentos(ValorLexico* valorLexico) {
    // essa chave ta
    // no escopo auxEscopo
    // e no simbolo auxSimbolo
    Simbolo* funcao = getPilhaGlobal()->escopo[auxEscopo]->simbolos[auxSimbolo];

    // os argumentos que estao sendo passados a chamada da funcao
    // estao no array argumentosLidos. Eles precisam bater com os da funcao
    
    // validando quantidade de argumentos
    if (funcao->qtdItems > indiceUltimoArgumento + 1) {
        TerminaComErro(ERR_MISSING_ARGS, funcao->chave, valorLexico->linha);
    } else if (funcao->qtdItems < indiceUltimoArgumento + 1) {
        TerminaComErro(ERR_EXCESS_ARGS, funcao->chave, valorLexico->linha);
    }

    // validando tipo dos argumentos
    for (int i = 0; i <= indiceUltimoArgumento; i++) {
        int tipoArg = argumentosLidos[i];
        if (!tiposSaoCompativeis(funcao->tiposArgumentos[i], tipoArg)) {
            TerminaComErro(ERR_WRONG_TYPE_ARGS, funcao->chave, valorLexico->linha);
        }
    }

    indiceUltimoArgumento = -1;
}


void validaTipoInput(ValorLexico* valorLexico) {
    // apenas int e float sao validos para comando input
    char* chave = getChave(valorLexico);
    Simbolo* item = getSimboloDaPilha(chave);

    if (item == NULL) {
        // essa situacao deve nunca acontecer, pois ja lemos um
        // identificador e o colocamos na pilha. 
        // Mas vamos nos manter seguros e manter essa verificacao.
        return;
    }

    if(item->tipo != TIPO_INT && item->tipo != TIPO_FLOAT) {
        TerminaComErro(ERR_WRONG_PAR_INPUT, chave, valorLexico->linha);
    }    
}

void validaTipoOutput(ValorLexico* valorLexico) {
    // apenas int e float sao validos para comando output
    char* chave = getChave(valorLexico);
    Simbolo* item = getSimboloDaPilha(chave);

    if (item == NULL) {
        // essa situacao deve nunca acontecer, pois ja lemos um
        // identificador e o colocamos na pilha. 
        // Mas vamos nos manter seguros e manter essa verificacao.
        return;
    }

    
    if(item->tipo != TIPO_INT && item->tipo != TIPO_FLOAT) {
        TerminaComErro(ERR_WRONG_PAR_OUTPUT, chave, valorLexico->linha);
    }
}

void validaTipoOutputLiteral(int tipo, int linha) {
    // apenas int e float sao validos para comando output
    if(!tiposSaoCompativeis(TIPO_INT, tipo) && !tiposSaoCompativeis(TIPO_FLOAT, tipo)) {
        TerminaComErro(ERR_WRONG_PAR_OUTPUT, NULL, linha);
    }
}

void validaTipoRetorno(int tipo, int linha) {
    if (!tiposSaoCompativeis(tipoDaFuncaoAtual,tipo)) {
        TerminaComErro(ERR_WRONG_PAR_RETURN, chaveDaFuncaoAtual, linha);
    }
}

void validaQtdShift(ValorLexico* valorLexico, int qtd) {
    if (qtd > 16) {
        TerminaComErro(ERR_WRONG_PAR_SHIFT, getChave(valorLexico), valorLexico->linha);
    }
}