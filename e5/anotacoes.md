## chamada de funcao

programa comeca sempre indo pra main()

### 
loadI 1024 => rfp // SEMPRE (aparentemente)
loadI 1024 => rsp // SEMPRE (aparentemente)
loadI 9 => rbss // aqui em vez de 9 eh (numero de linhas no programa) + 1

### label

`(letra)(qualquercoisa): instrucoes`

isso associa o label a linha que ele foi declarado
depois, para chamar, basta dar um jump pro label que ele vai pra linha onde foi declarado, ex:

jumpI -> label

ex:

```c
int soma(a, b) {
    return a + b;
}
```

illoc:

```iloc
soma:
```