#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "gerador_iloc.h"
#include "tabela_simbolos.h"
#include "ast.h"

#define MAX_COD_SIZE 500000

int rfp = 0;
int rsp = 0;

// comeca em 4 pq tem os 3 load do comeco e o jump pra main
int countInstrucoes = 4;

char bufferCodigoILOC[MAX_COD_SIZE];
char bufferIntermediario[MAX_COD_SIZE];

int indiceProximoRotulo = 0;
char* rotulosPorIndice[MAX_COD_SIZE];

// se a funcao 'func' estiver no indice 3, sera o L3:
char* rotulos[3000] ;

char* labelMain;

int indiceBuraco = 0;

int qtdFuncoesEncontradas = 0;

int adicionaBuraco() {    
    char temp[15];

    sprintf(temp, "${%d}", indiceBuraco);

    strcat(bufferCodigoILOC, temp);
    indiceBuraco++;

    return indiceBuraco;
}

void remenda(char* remendo, int indiceBuraco) {
    char buraco[15];
    sprintf(buraco, "${%d}", indiceBuraco);

    sprintf(bufferCodigoILOC, str_replace(bufferCodigoILOC, buraco, remendo));
}

void addToBuffer(char* text) {
    strcat(bufferCodigoILOC, text);
}

void addToBufferIntermediario(char* text) {
    strcat(bufferIntermediario, text);
}

char* getRegDoEscopo(Escopo escopo) {
    if(escopo == ESCOPO_GLOBAL) {
        return "rbss";
    } else {
        return "rfp";
    }
}

// gera uma unica inicializacao
// criar variavel com um valor 
// int x <= 14
void geraVariavelLocalComInicializacao(int deslocamento, int valor) {
    int tamanho = 4; // TODO receber como parametro
    
    int regValor = 0;

    char temp[100];
    
    // reserva espaco para a variavel
    sprintf(temp, "\taddI rsp, %d => rsp\n", tamanho);
    addToBufferIntermediario(temp);

    // coloca o valor no regValor
    sprintf(temp, "\tloadI %d => r%d\n", valor, regValor);
    addToBufferIntermediario(temp);

    // escreve o valor de regValor no endereco do regMemoria
    sprintf(temp, "\tstoreAI r%d => rfp, %d\n", regValor, deslocamento);
    addToBufferIntermediario(temp);

    countInstrucoes += 3;
}

void geraVariavelLocalSemInicializacao() {
    int tamanho = 4; // TODO receber como parametro

    char temp[30];    
    
    // reserva espaco para a variavel
    sprintf(temp, "\taddI rsp, %d => rsp\n", tamanho);
    addToBufferIntermediario(temp);

    countInstrucoes += 1;
}

void geraVariavelGlobalSemInicializacao() {
    int tamanho = 4; // TODO receber como parametro

    char temp[30];    
    
    // reserva espaco para a variavel
    sprintf(temp, "\taddI rbss, %d => rbss\n", tamanho);
    addToBufferIntermediario(temp);

    countInstrucoes += 1;
}

void geraAtribuicaoLiteral(int deslocamento, int valor, Escopo escopoDaVariavel) {
    int tamanho = 4; // TODO receber como parametro

    char temp[30];
    int regValor = 0;

    // coloca o valor no regValor
    sprintf(temp, "\tloadI %d => r%d\n", valor, regValor);
    addToBufferIntermediario(temp);

    char* reg = getRegDoEscopo(escopoDaVariavel);

    // escreve o valor de regValor no endereco do regMemoria
    sprintf(temp, "\tstoreAI r%d => %s, %d\n", regValor, reg, deslocamento);
    addToBufferIntermediario(temp);

    countInstrucoes += 2;
}

char* geraFuncao(char* nomeFuncao) {
    qtdFuncoesEncontradas++;

    char* label = geraRotulo();
    rotulosPorIndice[indiceProximoRotulo - 1] =  strdup(nomeFuncao);

    int qtdParametros = 0; // TODO passar por parametro

    // o rotulo
    addToBuffer(label);
    addToBuffer(":");    
    
    if (strcmp(nomeFuncao,"main") == 0) {
        labelMain = label;
    }
    // o i2i
    addToBuffer("\ti2i rsp => rfp\n");

    char* temp;
    // avancamos o SP depois de ter salvado dados de retorno nele
    sprintf(temp, "\taddI rsp, %d => rsp\n", 16 + qtdParametros * 4);
    addToBuffer(temp);

    addToBuffer(bufferIntermediario);

    bufferIntermediario[0] = '\0';

    return strdup(label);
}

void esvaziaBufferIntermediario() {
    addToBuffer(bufferIntermediario);

    bufferIntermediario[0] = '\0';
}

char* geraRotulo() {
    char temp[5];

    // jumpI antes da primeira funcao
    if (indiceProximoRotulo == 0) {
        adicionaBuraco();
    }


    sprintf(temp, "L%d", indiceProximoRotulo);

    indiceProximoRotulo++;

    return strdup(temp);
}

char* getRotulo(char* label) {
    printf("vou buscar o rotulo %s\n", label);
    
    for (int i = 0; i < indiceProximoRotulo; i++)
    {
        printf("comparando com %s\n", rotulosPorIndice[i]);
        if (strcmp(rotulosPorIndice[i], label) == 0) {
            char temp[5];
            sprintf(temp, "L%d", i);

            return strdup(temp);
        }
    }

    return NULL;
}

void geraChamadaFuncao(char* nomeFuncAlvo) {
    // sequencia de chamada:
    int qtdInstrucoesSequenciaDeChamada = 5;

    // carrega no r3 a instrucao de retorno (linhas comecam do indice 0)
    // loadI 20 => r3
    char* temp;
    sprintf(temp, "\tloadI %d => r3\n", qtdInstrucoesSequenciaDeChamada + countInstrucoes + (2 *  (qtdFuncoesEncontradas + 1)));
    addToBufferIntermediario(temp);
    addToBufferIntermediario("\tstoreAI r3 => rsp, 0\n");
    addToBufferIntermediario("\tstoreAI rsp => rsp, 4\n");
    addToBufferIntermediario("\tstoreAI rfp => rsp, 8\n");

    // obtem rotulo da func 'alvo'
    char* rotuloAlvo = getRotulo(nomeFuncAlvo);
    sprintf(temp, "\tjumpI => %s\n", rotuloAlvo);
    addToBufferIntermediario(temp);

    countInstrucoes += qtdInstrucoesSequenciaDeChamada;
}

// assume que o valor esta carregado em r0
void geraSequenciaDeRetorno(int qtdParametros) {
    char temp[100];
    // storeAI r0 => rfp, 12
    sprintf(temp, "\tstoreAI r0 => rfp, %d\n", 12 + 4 * qtdParametros);
    addToBufferIntermediario(temp);
    countInstrucoes++;

    // loadAI rfp, 0 => r0 	// obtém end. retorno
    addToBufferIntermediario("\tloadAI rfp, 0 => r0\n");
    countInstrucoes++;

    // loadAI rfp, 4 => r1 	// obtém rsp salvo
    addToBufferIntermediario("\tloadAI rfp, 4 => r1\n");
    countInstrucoes++;

    // loadAI rfp, 8 => r2 	// obtém rfp salvo -> pra saber pra onde voltar (o rfp salvo eh o escopo de quem chamou)
    addToBufferIntermediario("\tloadAI rfp, 8 => r2\n");
    countInstrucoes++;

    // store r1 => rsp		// seta o rsp atual pro rfp de quem chamou
    addToBufferIntermediario("\tstore r1 => rsp\n");
    countInstrucoes++;

    // store r2 => rfp		// seta o rfp atual pro rfp de quem chamou (essa instrucao, junto com a de cima, desempilha o frame)
    addToBufferIntermediario("\tstore r2 => rfp\n");
    countInstrucoes++;

    addToBufferIntermediario("\tjump => r0\n");
    countInstrucoes++;
}

void imprimeCodigo() {
    esvaziaBufferIntermediario();

    char codigoInicial[100];
    // 2 = 1 do halt e + 1 pra comecar depois do codigo

    sprintf(codigoInicial, "\tloadI 1024 => rfp \n\tloadI 1024 => rsp \n\tloadI %d =>  rbss", countInstrucoes + 1 + (2 *  (qtdFuncoesEncontradas)));

    char* jump = malloc(sizeof(char) * 100);
    sprintf(jump, "\tjumpI => %s\n", labelMain);

    remenda(jump, 0);
    
    printf("%s\n%s\thalt\n", codigoInicial, bufferCodigoILOC);
}

// source: https://stackoverflow.com/questions/779875/what-function-is-to-replace-a-substring-from-a-string-in-c
char *str_replace(char *orig, char *rep, char *with) {
    char *result; // the return string
    char *ins;    // the next insert point
    char *tmp;    // varies
    int len_rep;  // length of rep (the string to remove)
    int len_with; // length of with (the string to replace rep with)
    int len_front; // distance between rep and end of last rep
    int count;    // number of replacements

    // sanity checks and initialization
    if (!orig || !rep)
        return NULL;
    len_rep = strlen(rep);
    if (len_rep == 0)
        return NULL; // empty rep causes infinite loop during count
    if (!with)
        with = "";
    len_with = strlen(with);

    // count the number of replacements needed
    ins = orig;
    for (count = 0; tmp = strstr(ins, rep); ++count) {
        ins = tmp + len_rep;
    }

    tmp = result = malloc(strlen(orig) + (len_with - len_rep) * count + 1);

    if (!result)
        return NULL;

    // first time through the loop, all the variable are set correctly
    // from here on,
    //    tmp points to the end of the result string
    //    ins points to the next occurrence of rep in orig
    //    orig points to the remainder of orig after "end of rep"
    while (count--) {
        ins = strstr(orig, rep);
        len_front = ins - orig;
        tmp = strncpy(tmp, orig, len_front) + len_front;
        tmp = strcpy(tmp, with) + len_with;
        orig += len_front + len_rep; // move to next "end of rep"
    }
    strcpy(tmp, orig);
    return result;
}
