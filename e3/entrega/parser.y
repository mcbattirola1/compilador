%{
#include <stdio.h>
#include "ast.h"

int yylex(void);
int yyerror (char const *s);
int get_line_number();

extern void *arvore;
%}

%union {
	struct ValorLexico *valorLexico;
	struct Nodo *nodo;
}

%start programa

%token TK_PR_INT
%token TK_PR_FLOAT
%token TK_PR_BOOL
%token TK_PR_CHAR
%token TK_PR_STRING
%token<valorLexico> TK_PR_IF
%token TK_PR_THEN
%token<valorLexico> TK_PR_ELSE
%token<valorLexico> TK_PR_WHILE
%token TK_PR_DO
%token<valorLexico> TK_PR_INPUT
%token<valorLexico> TK_PR_OUTPUT
%token<valorLexico> TK_PR_RETURN
%token TK_PR_CONST
%token TK_PR_STATIC
%token TK_PR_FOREACH
%token<valorLexico> TK_PR_FOR
%token TK_PR_SWITCH
%token TK_PR_CASE
%token<valorLexico> TK_PR_BREAK
%token<valorLexico> TK_PR_CONTINUE
%token TK_PR_CLASS
%token TK_PR_PRIVATE
%token TK_PR_PUBLIC
%token TK_PR_PROTECTED
%token TK_PR_END
%token TK_PR_DEFAULT
%token<valorLexico> TK_OC_LE
%token<valorLexico> TK_OC_GE
%token<valorLexico> TK_OC_EQ
%token<valorLexico> TK_OC_NE
%token<valorLexico> TK_OC_AND
%token<valorLexico> TK_OC_OR
%token<valorLexico> TK_OC_SL
%token<valorLexico> TK_OC_SR
%token<valorLexico> TK_LIT_INT
%token<valorLexico> TK_LIT_FLOAT
%token<valorLexico> TK_LIT_FALSE
%token<valorLexico> TK_LIT_TRUE
%token<valorLexico> TK_LIT_CHAR
%token<valorLexico> TK_LIT_STRING
%token<valorLexico> TK_IDENTIFICADOR
%token<valorLexico> '+'
%token<valorLexico> '-'
%token<valorLexico> '*'
%token<valorLexico> '/'
%token<valorLexico> '>'
%token<valorLexico> '<'
%token<valorLexico> '%'
%token<valorLexico> '|'
%token<valorLexico> '&'
%token<valorLexico> '^'
%token<valorLexico> '#'
%token<valorLexico> '?'
%token<valorLexico> '!'
%token TOKEN_ERRO

%type<nodo> id
%type<nodo> programa
%type<nodo> funcao
%type<nodo> bloco_comandos
%type<nodo> lista_comandos
%type<nodo> comando
%type<nodo> atribuicao
%type<nodo> id_array
%type<nodo> expressao
%type<nodo> literal
%type<nodo> literal_numerico
%type<nodo> cabecalho_funcao
%type<nodo> input
%type<nodo> output
%type<nodo> declaracao_variaveis_locais
%type<nodo> variavel_local
%type<nodo> lista_variaveis_locais
%type<nodo> chamada_funcao
%type<nodo> lista_argumentos
%type<nodo> if
%type<nodo> for
%type<nodo> while
%type<nodo> shift
%type<nodo> retorno
%type<nodo> break
%type<nodo> continue
%type<nodo> comando_com_bloco

%left "ternario"
%left "binario"
%left "unario"

%left TK_OC_OR
%left TK_OC_AND
%left '|'
%left '^'
%left TK_OC_SL TK_OC_SR
%left TK_OC_EQ TK_OC_NE
%left '>' TK_OC_LE '<' TK_OC_GE  
%left '+' '-' '!' '#'  '?' '&' '%'  
%left '*' '/'

%%

programa: variavel_global programa { $$ = $2; }
| funcao programa { $1->filhos[0] = $2; $$ = $1; arvore = (void*) $$;}
| /* vazio */ { $$ = NULL; } ;

tipo: TK_PR_INT | TK_PR_FLOAT | TK_PR_BOOL | TK_PR_CHAR | TK_PR_STRING ;

literal: TK_LIT_CHAR { $$ = criaNodo(LITERAL, $1, 0, 0, 0, 0, 0); }
| TK_LIT_FALSE { $$ = criaNodo(LITERAL, $1, 0, 0, 0 ,0 ,0);}
| TK_LIT_FLOAT  { $$ = criaNodo(LITERAL, $1, 0, 0, 0 ,0 ,0); }
| TK_LIT_INT { $$ = criaNodo(LITERAL, $1, 0, 0, 0 ,0 ,0); }
| TK_LIT_STRING { $$ = criaNodo(LITERAL, $1, 0, 0, 0, 0, 0); }
| TK_LIT_TRUE { $$ = criaNodo(LITERAL, $1, 0, 0, 0 ,0 ,0); } ;

literal_numerico: TK_LIT_FLOAT { $$ = criaNodo(LITERAL, $1, 0, 0,0 ,0 ,0); }
| TK_LIT_INT { $$ = criaNodo(LITERAL, $1, 0, 0,0 ,0 ,0); } ;

static_opcional: TK_PR_STATIC | /* vazio */ ;
const_opcional: TK_PR_CONST | /* vazio */ ;
array_opcional: '[' TK_LIT_INT ']' | '[' '+' TK_LIT_INT ']' | /* vazio */ ;

lista_nome_var_global: nome_var_global ',' lista_nome_var_global | nome_var_global;
nome_var_global: id array_opcional;

variavel_global: static_opcional tipo lista_nome_var_global ';' ;

funcao: cabecalho_funcao bloco_comandos { $$ = criaNodo(FUNCAO, $1->valorLexico, 0, $2, 0, 0, 0);};

cabecalho_funcao: static_opcional tipo id '(' lista_parametros ')' {$$ = $3;} ;
parametro: const_opcional tipo id ;
lista_parametros: parametro ',' lista_parametros | parametro | /* vazio */ ;

inicio_bloco_comandos: '{' ;
fim_bloco_comandos: '}' ;
bloco_comandos: inicio_bloco_comandos lista_comandos fim_bloco_comandos { $$ = $2; } | '{' '}' { $$ = NULL; } ;

comando: declaracao_variaveis_locais { $$ = $1; } 
| atribuicao { $$ = $1; }
| input { $$ = $1; }
| output { $$ = $1; }
| chamada_funcao  { $$ = $1; }
| shift { $$ = $1; }
| retorno { $$ = $1; }
| break { $$ = $1; }
| continue { $$ = $1; }
| bloco_comandos { $$ = $1; };

comando_com_bloco: if { $$ = $1; }
| for { $$ = $1; }
| while { $$ = $1; } ;

break: TK_PR_BREAK { $$ = criaNodo(COMANDO, $1, 0, 0, 0, 0, 0); }
continue: TK_PR_CONTINUE { $$ = criaNodo(COMANDO, $1, 0, 0, 0, 0, 0); }

lista_comandos: comando ';' lista_comandos { 
    // TODO: refatorar
    if ($$ == NULL) {
      $$ = $3;
    } else {
      Nodo* aux = $$;
      while(aux->filhos[0] != NULL) {
        aux = aux->filhos[0];
      }
      aux->filhos[0] = $3;
      $$ = $1;
    }
  }
| comando ';' { $$ = $1; } 
| comando_com_bloco { $$ = $1; }
| comando_com_bloco lista_comandos {
  if ($$ == NULL) {
      $$ = $2;
    } else {
      Nodo* aux = $$;
      while(aux->filhos[0] != NULL) {
        aux = aux->filhos[0];
      }
      aux->filhos[0] = $2;
      $$ = $1;
    }
 } ;

variavel_local: id { $$ = NULL; }
| id TK_OC_LE literal { $$ = criaNodo(INICIALIZACAO, $2, 0, $1, $3, 0, 0); }
| id TK_OC_LE id { $$ = criaNodo(INICIALIZACAO, $2, 0, $1, $3, 0, 0); };

lista_variaveis_locais: variavel_local ',' lista_variaveis_locais {
  if($1 == NULL) {
    $$ = $3;
  } else {
    $1->filhos[0] = $3; $$ = $1; 
  }
}
| variavel_local { $$ = $1; } ;

declaracao_variaveis_locais: static_opcional const_opcional tipo lista_variaveis_locais { 
    $$ = $4;  
  };

id: TK_IDENTIFICADOR { $$ = criaNodo(IDENTIFICADOR, $1, 0, 0, 0, 0, 0); }; 

expressao: id '[' expressao ']' { $$ = criaNodo(ACESSO_ARRAY, 0, 0, $1, $3, 0, 0); }
  | id { $$ = $1; }
  | TK_LIT_TRUE { $$ = criaNodo(LITERAL, $1, 0, 0, 0, 0, 0); }
  | TK_LIT_FALSE { $$ = criaNodo(LITERAL, $1, 0, 0, 0, 0, 0); }
  | literal_numerico { $$ = $1; }
  | chamada_funcao { $$ = $1; }
  | '(' expressao ')' { $$ = $2; }
  | '-' expressao { $$ = criaNodo(OP_UNARIA, $1, 0, $2, 0, 0, 0); }
  | '+' expressao { $$ = criaNodo(OP_UNARIA, $1, 0, $2, 0, 0, 0); }
  | '!' expressao { $$ = criaNodo(OP_UNARIA, $1, 0, $2, 0, 0, 0); }
  | '&' expressao { $$ = criaNodo(OP_UNARIA, $1, 0, $2, 0, 0, 0); }
  | '*' expressao { $$ = criaNodo(OP_UNARIA, $1, 0, $2, 0, 0, 0); }
  | '?' expressao { $$ = criaNodo(OP_UNARIA, $1, 0, $2, 0, 0, 0); }
  | '#' expressao { $$ = criaNodo(OP_UNARIA, $1, 0, $2, 0, 0, 0); }
  | expressao '+' expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0);}
  | expressao '-' expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0);}
  | expressao '*' expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0);}
  | expressao '/' expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0);}
  | expressao '%' expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0);}
  | expressao '|' expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0);}
  | expressao '&' expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0);}
  | expressao '^' expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0);}
  | expressao '>' expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0);}
  | expressao '<' expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0);}
  | expressao TK_OC_LE expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0);}
  | expressao TK_OC_GE expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0);}
  | expressao TK_OC_EQ expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0);}
  | expressao TK_OC_NE expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0);}
  | expressao TK_OC_AND expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0);}
  | expressao TK_OC_OR expressao { $$ = criaNodo(OP_BINARIA, $2, 0, $1, $3, 0, 0);}
  | expressao '?' expressao ':' expressao %prec "ternario" ;

id_array:  id '[' expressao ']' {$$ = criaNodo(ACESSO_ARRAY, 0, 0, $1, $3, 0, 0); } ;

atribuicao: id '=' expressao { $$ = criaNodo(ATRIBUICAO, 0, 0, $1, $3, 0, 0); }
| id_array '=' expressao {$$ = criaNodo(ATRIBUICAO, 0, 0, $1, $3, 0, 0); };

input: TK_PR_INPUT id { $$ = criaNodo(COMANDO, $1, 0, $2, 0, 0, 0);};
output: TK_PR_OUTPUT id { $$ = criaNodo(COMANDO, $1, 0, $2, 0, 0, 0);} 
| TK_PR_OUTPUT literal { $$ = criaNodo(COMANDO, $1, 0, $2, 0, 0, 0);}
| TK_PR_OUTPUT '-' literal_numerico { Nodo* novoNodo = criaNodo(OP_UNARIA, $2, 0, $3, 0, 0, 0); $$ = criaNodo(COMANDO, $1, 0, novoNodo, 0, 0, 0);} ;

chamada_funcao: id '(' lista_argumentos ')' { $$ = criaNodo(CHAMADA_FUNCAO, $1->valorLexico, 0, $3, 0, 0, 0);};
lista_argumentos: expressao ',' lista_argumentos { $1->filhos[0] = $3; $$ = $1; }
| expressao { $$ = $1; }
| { $$ = NULL; } /* vazio */;

if: TK_PR_IF '(' expressao ')' bloco_comandos { $$ = criaNodo(COMANDO, $1, 0, $3, $5, 0, 0); }
| TK_PR_IF '(' expressao ')' bloco_comandos TK_PR_ELSE bloco_comandos { 
    Nodo* nodoElse = criaNodo(COMANDO, $6, 0, $7, 0, 0, 0);
    $$ = criaNodo(COMANDO, $1, 0, $3, $5, $7, 0); 
  } ;

for: TK_PR_FOR '(' atribuicao ':' expressao ':' atribuicao ')' bloco_comandos  {
  $$ = criaNodo(COMANDO, $1, 0, $3, $5, $7, $9);
};

while: TK_PR_WHILE '(' expressao ')' TK_PR_DO bloco_comandos {
  $$ = criaNodo(COMANDO, $1, 0, $3, $6, 0, 0);
};

shift: id '[' expressao ']' TK_OC_SL TK_LIT_INT {
  Nodo* nodoAcesso = criaNodo(ACESSO_ARRAY, 0, 0, $1, $3, 0, 0);
  Nodo* nodoLitInt = criaNodo(LITERAL, $6, 0, 0,0 ,0 ,0);
  $$ = criaNodo(COMANDO, $5, 0, nodoAcesso, nodoLitInt, 0, 0);
 }
| id TK_OC_SL TK_LIT_INT {
  Nodo* nodoLitInt = criaNodo(LITERAL, $3, 0, 0,0 ,0 ,0);
  $$ = criaNodo(COMANDO, $2, 0, $1, nodoLitInt, 0, 0);
}
| id '[' expressao ']' TK_OC_SR TK_LIT_INT {
  Nodo* nodoAcesso = criaNodo(ACESSO_ARRAY, 0, 0, $1, $3, 0, 0);
  Nodo* nodoLitInt = criaNodo(LITERAL, $6, 0, 0,0 ,0 ,0);
  $$ = criaNodo(COMANDO, $5, 0, nodoAcesso, nodoLitInt, 0, 0);
}
| id TK_OC_SR TK_LIT_INT {
  Nodo* nodoLitInt = criaNodo(LITERAL, $3, 0, 0,0 ,0 ,0);
  $$ = criaNodo(COMANDO, $2, 0, $1, nodoLitInt, 0, 0);
};

retorno: TK_PR_RETURN expressao { $$ = criaNodo(COMANDO, $1, 0, $2, 0, 0, 0); };

%%

int yyerror(char const *s) {
    printf("error on line %d: %s\n", get_line_number(), s);
    return 1;
}
