# TODO - etapa 3
[x] Associação de valor ao token (yylval) (variável valor_lexico)
[x] Criar tipo struct para o valor_lexico
[x] Implementar árvore
[x] Exportar árvore
[x] Função *libera*
[ ] Sem vazamento de memória
[x] Corrigir Ordem das operacoes (multiplicacao antes da soma)
[x] Multiplas inicializacoes, nao colocar as sem valor na arvore.
[x] Correção de erros da etapa anterior

# TODO - etapa 2

[x] Implementar regra ```expressao_logica```
[x] Implementar regra ```expressao_aritmetica```
[x] Associatividade nas expressoes
[x] Parenteses nas expressoes
[x] If then else
[x] For
[x] While
[x] Em caso de erro, imprimir mensagem com a linha que gerou o erro e informacoes adicionais.
