%{
#include <stdio.h>

int yylex(void);
int yyerror (char const *s);
int get_line_number();
%}

%start programa

%token TK_PR_INT
%token TK_PR_FLOAT
%token TK_PR_BOOL
%token TK_PR_CHAR
%token TK_PR_STRING
%token TK_PR_IF
%token TK_PR_THEN
%token TK_PR_ELSE
%token TK_PR_WHILE
%token TK_PR_DO
%token TK_PR_INPUT
%token TK_PR_OUTPUT
%token TK_PR_RETURN
%token TK_PR_CONST
%token TK_PR_STATIC
%token TK_PR_FOREACH
%token TK_PR_FOR
%token TK_PR_SWITCH
%token TK_PR_CASE
%token TK_PR_BREAK
%token TK_PR_CONTINUE
%token TK_PR_CLASS
%token TK_PR_PRIVATE
%token TK_PR_PUBLIC
%token TK_PR_PROTECTED
%token TK_PR_END
%token TK_PR_DEFAULT
%token TK_OC_LE
%token TK_OC_GE
%token TK_OC_EQ
%token TK_OC_NE
%token TK_OC_AND
%token TK_OC_OR
%token TK_OC_SL
%token TK_OC_SR
%token TK_LIT_INT
%token TK_LIT_FLOAT
%token TK_LIT_FALSE
%token TK_LIT_TRUE
%token TK_LIT_CHAR
%token TK_LIT_STRING
%token TK_IDENTIFICADOR
%token TOKEN_ERRO

%left "ternario"
%left "binario"
%left "unario"

%left '+' '-' '!' '#' '*' '?' '&' '/' '%' '|' '^' '>' TK_OC_LE '<' TK_OC_GE TK_OC_EQ TK_OC_NE TK_OC_AND TK_OC_OR TK_OC_SL TK_OC_SR

%%

programa: variavel_global programa | funcao programa | /* vazio */ ;

tipo: TK_PR_INT | TK_PR_FLOAT | TK_PR_BOOL | TK_PR_CHAR | TK_PR_STRING ;
literal: TK_LIT_CHAR | TK_LIT_FALSE | TK_LIT_FLOAT | TK_LIT_INT | TK_LIT_STRING | TK_LIT_TRUE ;
literal_numerico: TK_LIT_FLOAT | TK_LIT_INT ;
static_opcional: TK_PR_STATIC | /* vazio */ ;
const_opcional: TK_PR_CONST | /* vazio */ ;
array_opcional: '[' TK_LIT_INT ']' | '[' '+' TK_LIT_INT ']' | /* vazio */ ;

lista_nome_var_global: nome_var_global ',' lista_nome_var_global | nome_var_global;
nome_var_global: TK_IDENTIFICADOR array_opcional;

variavel_global: static_opcional tipo lista_nome_var_global ';' ;

funcao: cabecalho_funcao bloco_comandos;

cabecalho_funcao: static_opcional tipo TK_IDENTIFICADOR '(' lista_parametros ')' ;
parametro: const_opcional tipo TK_IDENTIFICADOR ;
lista_parametros: parametro ',' lista_parametros | parametro | /* vazio */ ;

bloco_comandos: '{' lista_comandos '}' | '{' '}' ;
comando: variavel_local 
| atribuicao 
| input 
| output 
| chamada_funcao 
| shift 
| retorno 
| TK_PR_BREAK 
| TK_PR_CONTINUE 
| bloco_comandos ;

comando_com_bloco: if 
| for 
| while ;

lista_comandos: comando ';' lista_comandos 
 | comando ';' 
 | comando_com_bloco
 | comando_com_bloco lista_comandos ;

inicializacao_opcional: TK_OC_LE literal | TK_OC_LE TK_IDENTIFICADOR | /* vazio */; 
nome_var_local: TK_IDENTIFICADOR inicializacao_opcional ;
lista_nome_var_local: nome_var_local ',' lista_nome_var_local | nome_var_local ;
variavel_local: static_opcional const_opcional tipo lista_nome_var_local ;

expressao: TK_IDENTIFICADOR '[' expressao ']' 
  | TK_IDENTIFICADOR 
  | TK_LIT_TRUE
  | TK_LIT_FALSE
  | literal_numerico 
  | chamada_funcao 
  | '(' expressao ')'
  | '-' expressao
  | '+' expressao
  | '!' expressao
  | '&' expressao
  | '*' expressao
  | '?' expressao
  | '#' expressao
  | expressao '+' expressao 
  | expressao '-' expressao 
  | expressao '*' expressao 
  | expressao '/' expressao 
  | expressao '%' expressao 
  | expressao '|' expressao 
  | expressao '&' expressao 
  | expressao '^' expressao 
  | expressao '>' expressao 
  | expressao '<' expressao 
  | expressao TK_OC_LE expressao 
  | expressao TK_OC_GE expressao 
  | expressao TK_OC_EQ expressao 
  | expressao TK_OC_NE expressao
  | expressao TK_OC_AND expressao 
  | expressao TK_OC_OR expressao   
  | expressao '?' expressao ':' expressao %prec "ternario" ;

atribuicao: TK_IDENTIFICADOR '=' expressao | TK_IDENTIFICADOR '[' expressao ']' '=' expressao ; 

input: TK_PR_INPUT TK_IDENTIFICADOR ;
output: TK_PR_OUTPUT TK_IDENTIFICADOR | TK_PR_OUTPUT literal | TK_PR_OUTPUT '-' literal_numerico ;

chamada_funcao: TK_IDENTIFICADOR '(' lista_argumentos ')';
lista_argumentos: expressao ',' lista_argumentos | expressao | /* vazio */;

if: TK_PR_IF '(' expressao ')' bloco_comandos
| TK_PR_IF '(' expressao ')' bloco_comandos TK_PR_ELSE bloco_comandos ;

for: TK_PR_FOR '(' atribuicao ':' expressao ':' atribuicao ')' bloco_comandos ;

while: TK_PR_WHILE '(' expressao ')' TK_PR_DO bloco_comandos ;

shift: TK_IDENTIFICADOR '[' expressao ']' TK_OC_SL TK_LIT_INT
| TK_IDENTIFICADOR TK_OC_SL TK_LIT_INT
| TK_IDENTIFICADOR '[' expressao ']' TK_OC_SR TK_LIT_INT
| TK_IDENTIFICADOR TK_OC_SR TK_LIT_INT ;

retorno: TK_PR_RETURN expressao ;

%%

int yyerror(char const *s) {
    printf("error on line %d: %s\n", get_line_number(), s);
    return 1;
}
