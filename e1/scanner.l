%{
#include "tokens.h"
%}

%option yylineno
DIGITO [0-9]+
INT (\+|-)?{DIGITO}
IDENTIFICADOR ([a-zA-Z]|_)+([a-zA-Z]|[0-9]|_)*
%x COMMENT

%%
\n {}
"//".*  {}
[ \t\r]+ {}

"/*"    {BEGIN(COMMENT);}
<COMMENT>"*/" {BEGIN(INITIAL);}
<COMMENT>"\n" {}
<COMMENT>.


"int" { return TK_PR_INT; }
"float" { return TK_PR_FLOAT; }
"bool" { return TK_PR_BOOL; }
"char" { return TK_PR_CHAR; }
"string" { return TK_PR_STRING; }
"if" { return TK_PR_IF; }
"then" { return TK_PR_THEN; }
"else" { return TK_PR_ELSE; }
"while" { return TK_PR_WHILE; }
"do" { return TK_PR_DO; }
"input" { return TK_PR_INPUT; }
"output" { return TK_PR_OUTPUT; }
"return" { return TK_PR_RETURN; }
"const" { return TK_PR_CONST; }
"static" { return TK_PR_STATIC; }
"foreach" { return TK_PR_FOREACH; }
"for" { return TK_PR_FOR; }
"switch" { return TK_PR_SWITCH; }
"case" { return TK_PR_CASE; }
"break" { return TK_PR_BREAK; }
"continue" { return TK_PR_CONTINUE; }
"class" { return TK_PR_CLASS; }
"private" { return TK_PR_PRIVATE; }
"public" { return TK_PR_PUBLIC; }
"protected" { return TK_PR_PROTECTED; }
"end" { return TK_PR_END; }
"default" { return TK_PR_DEFAULT; }

"," { return yytext[0]; }
";" { return yytext[0]; }
":" { return yytext[0]; }
"(" { return yytext[0]; }
")" { return yytext[0]; }
"[" { return yytext[0]; }
"]" { return yytext[0]; }
"{" { return yytext[0]; }
"}" { return yytext[0]; }
"+" { return yytext[0]; }
"-" { return yytext[0]; }
"|" { return yytext[0]; }
"*" { return yytext[0]; }
"/" { return yytext[0]; }
"<" { return yytext[0]; }
">" { return yytext[0]; }
"=" { return yytext[0]; }
"!" { return yytext[0]; }
"&" { return yytext[0]; }
"%" { return yytext[0]; }
"#" { return yytext[0]; }
"^" { return yytext[0]; }
"." { return yytext[0]; }
"$" { return yytext[0]; }

"<=" { return TK_OC_LE; }
">=" { return TK_OC_GE; }
"==" { return TK_OC_EQ; }
"!=" { return TK_OC_NE; }
"&&" { return TK_OC_AND; }
"||" { return TK_OC_OR; }
">>" { return TK_OC_SL; }
"<<" { return TK_OC_SR; }


false { return TK_LIT_FALSE; }
true { return TK_LIT_TRUE; }

'.' { return TK_LIT_CHAR; }
'..+' { return TOKEN_ERRO; }
'[^'|^\n]* { return TOKEN_ERRO; }


\".*\" { return TK_LIT_STRING; }
\"[^"|^\n]* { return TOKEN_ERRO; }

{INT} { return TK_LIT_INT; }

{INT}\.{DIGITO}+((e|E){DIGITO})? { return TK_LIT_FLOAT; }
{INT}\.[^{DIGITO}] { return TOKEN_ERRO; }

{IDENTIFICADOR} { return TK_IDENTIFICADOR; }
{DIGITO}{IDENTIFICADOR} { return TOKEN_ERRO; }

. { return TOKEN_ERRO; }
%%

int get_line_number() {
    return yylineno;
}