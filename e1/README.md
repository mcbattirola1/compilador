# Compilador

## Dependencies

- GCC
- Flex
- Make

## How to compile

```make```

## Compile and run

```make && ./etapa1```
